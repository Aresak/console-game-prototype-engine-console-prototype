<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 29.05.2016
 * Time: 17:37
 */

?>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<title>Game Simulator</title>


<div id="console">
    <div id="console_window">
    </div>
    <div id="console_input_div">
        <input type="text" id="console_input">
    </div>
</div>

<script>
    var bruteforcestyle = "";
    var game_sessionkey = null;
    var refresher = 500;
    var server_id = null;
    var game_id = null;
    var helpStage = 0;

    var hasOpponent = false;


    var queueDetailsRefresh = 1000;
    var queueRefreshRefresh = 1000;


    $("html").keyup(function(event){
        if(event.keyCode == 13){
            sendCommand();
        }
    });

    newContent("> Welcome to Battle For Treasure Console - Type /help for help and start off!", "success");

    function generalHelp() {
        newContent("=== CONNECTION ===", "hint");
        newContent("/login (username/ID) - Login into account with username or ID", "hint");
        newContent("/connect (Game) - Connects to an server", "hint");
        newContent("/disconnect - Disconnects from the server", "hint");

        newContent("=== QUEUE ===", "hint");
        newContent("/queue join - Joins a queue", "hint");
        newContent("/queue leave - Leaves a queue", "hint");
        newContent("/queue details [refresh time/stop] - Shows details about yourself being in queue (can be automated)", "hint");
        newContent("/queue refresh [refresh time/stop] - Checks if the game is available for you or not (can be automated)", "hint");

        newContent("=== OPPONENT ===", "hint");
        newContent("/opponent details - Print out details about the opponent", "hint");

        newContent("=== CLIENT ===", "hint");
        newContent("/refresher (ms) - Changes the reach time between the client and the server (Default 500)", "hint");
        newContent("/clear - Clears the screen", "hint");
        newContent("/help [general/game] - Displays help lines", "hint");
        newContent("? - Tells you what to do next ;)", "hint");
    }

    function gameHelp() {
        newContent("============");
        newContent("=== PLAY ===", "hint");
        newContent("============");
        newContent("/faction select [ID/name] - Selects an faction for you!", "hint");
        newContent("/faction list - Shows a list of available factions", "hint");
        newContent("/faction select random - Selects a random faction for you", "hint");

        newContent("/ready - Makes you ready for the game", "hint");

        newContent("/stats - Displays your in game stats", "hint");
        newContent("/stats gold - Displays how much gold you have", "hint");
        newContent("/stats population - Displays the population info", "hint");
        newContent("/turn - Prints out who's turn is this", "hint");

        newContent("/map table - Prints a map out for you", "hint");

        newContent("/tile build [Building] [X] [Y] - Starts off a building on tile", "hint");
        newContent("/tile recruit [Unit] [X] [Y] - Recruits an unit on your tile", "hint");
        newContent("/tile move [Unit ID] [X] [Y] - Moves a unit to your designed tile", "hint");
    }

    var queueRefreshAutoEnabled = false;
    var queueRefreshAutoDestroy = false;
    function queueRefreshAuto() {
        if(queueRefreshAutoDestroy) {
            queueRefreshAutoDestroy = false;
            queueRefreshAutoEnabled = false;
            return;
        }
        queueRefreshAutoEnabled = true;
        sendToServer("/queue refresh", handlers.command);
        setTimeout(queueRefreshAuto, queueRefreshRefresh);
    }

    var queueDetailsAutoEnabled = false;
    var queueDetailsAutoDestroy = false;
    function queueDetailsAuto() {
        if(queueDetailsAutoDestroy) {
            queueDetailsAutoDestroy = false;
            queueDetailsAutoEnabled = false;
            return;
        }
        queueDetailsAutoEnabled = true;
        sendToServer("/queue details", handlers.command);
        setTimeout(queueDetailsAuto, queueDetailsRefresh);
    }

    function sendCommand() {
        var input = $("#console_input").val();
        $("#console_input").val("");

        console.log("Working on command");

        if(input.length == 0) {
            newContent("undefined (Type [/help] or [/help game] or [/help general]", "undefined");
            console.log("Undefined command");
            return;
        }

        if(input.split("")[0] == "/") {
            // an command

            var args = input.split(" ");
            switch(args[0]) {
                case "/clear":
                    $("#console_window").html("");
                    break;
                case "/refresher":
                    if(args[1] == undefined) {
                        newContent("Missing 1 argument /refresher [ms]!", "error");
                        break;
                    }
                    refresher = args[1];
                    newContent("Refresh rate has changed to " + args[1] + "ms", "success");
                    break;
                case "/help":
                    if(args[1] == "general") generalHelp();
                    else if(args[1] == "game") gameHelp();
                    else {
                        generalHelp();
                        gameHelp();
                    }
                    break;
                default:

                    if(args[0] == "/queue" && args[1] == "refresh" && args[2] != null) {
                        if(queueRefreshAutoEnabled) {
                            queueRefreshAutoDestroy = true;
                            newContent("Automatic queue refreshing disabled!", "warning");
                            break;
                        }
                        else {
                            newContent("Automatic queue refreshing enabled!", "success");
                            queueRefreshRefresh = args[2];
                            queueRefreshAuto();
                            break;
                        }
                    }
                    else if(args[0] == "/queue" && args[1] == "details" && args[2] != null) {
                        if(queueDetailsAutoEnabled) {
                            queueDetailsAutoDestroy = true;
                            newContent("Automatic queue details disabled!", "warning");
                            break;
                        }
                        else {
                            newContent("Automatic queue details enabled!", "success");
                            queueDetailsRefresh = args[2];
                            queueDetailsAuto();
                            break;
                        }
                    }

                    sendToServer(input, handlers.command);
                    break;
            }

        }
        else if(input.split("")[0] == "?") {
            // HELP LINES!
            switch(helpStage) {
                case 0:
                    newContent("You are in the first stage, and you are not logged in! Type /login [username/ID] to access", "hint");
                    newContent("and connect your account for this session. You can always use the ? for new help, I can tell you something every time ;)", "hint");
                    break;
                case 1:
                    newContent("You are logged in, just join the queue to get into the game /queue join", "hint");
                    newContent("You can always check the queue details by /queue details", "hint");
                    break;
                case 2:
                    newContent("You are waiting in queue, type /queue details for details", "hint");
                    newContent("You can always leave the queue by /queue leave", "hint");
                    break;

                case 10:
                    newContent("Check if it's your turn (/turn) and if so, select the faction! /faction select <random>", "hint");
                    break;
            }
        }
        else {
            // an message
            if(game_sessionkey == null) {
                newContent("You are not connected to the server yet! You cannot send a message", "error");
                return;
            }
            // newContent(userData.display_name + ": " + input, "player");
            input = "msg " + input;
            sendToServer(input, handlers.command);
        }
    }


    var emotesLook   = new Array();
    var emotesReplace = new Array();
    var userData = null;
    var handlers = {
        message: function(output) {
            newContent(output, "message");
        },
        command: function(output) {
            if(isJSON(output)) var json = JSON.parse(output);
            else {
                newContent(output);
                return;
            }

            switch(json.output.command) {
                case "login":
                    if(json.output.data.ID != undefined) {
                        newContent("Connected as " + json.output.data.display_name + "!", "server");
                        newContent("> ---", "hint");
                        newContent("> You are now allowed to connect to the servers!", "hint");
                        userData = json.output.data;
                        game_sessionkey = json.output.data.game_sessionkey;
                        helpStage = 1;
                    }
                    else console.error("Error: " + json);
                    break;

                case "queue_join":
                    helpStage = 2;
                    newContent("You have joined the queue. Just relax now and wait for your turn!", "server");
                    newContent("> --- hint", "hint");
                    newContent("> Do '/queue refresh' or '/queue refresh auto' because if you don't, you will be timed out from the queue", "hint");
                    break;

                case "queue_details":
                    newContent("Queue Details:<br>Total: "
                        + json.output.data.total + (json.output.data.total == 1 ? " Player" : " Players") +
                        "<br>Waiting: " + json.output.data.waiting + (json.output.data.waiting == 1 ? " Player" : " Players") +
                        "<br>Selecting: " + json.output.data.selecting + (json.output.data.selecting == 1 ? " Player" : " Players") +
                        "<br>Longest waiting time now: " + json.output.data.waitingtime + (json.output.data.waitingtime == 1 ? " second" : " seconds")
                        , "warning");
                    break;

                case "queue_refresh":
                    newContent("Reached new queue refresh. " + (json.output.data.game == 0 ? "Still haven't found a game tho" : "The game has been found for you!"), "server");
                    if(json.output.data.game != 0) {
                        // The game has been found
                        newContent("Auto-Connecting to Game #" + json.output.data.game, "server");
                        connect(json.output.data.game);
                    }
                    break;

                case "queue_leave":
                    newContent("You have left the queue!", "server");
                    helpStage = 1;
                    break;

                case "connect":
                    newContent("You have been connected to the server!", "success");
                    game_id = json.output.game.game_id;
                    server_id = json.output.game.server_id;
                    helpStage = 10;
                    break;

                case "disconnect":
                    newContent("You have been disconnected from the server!", "server");
                    helpStage = 1;
                    game_id = null;
                    server_id = null;
                    break;

                case "message":
                    newContent(json.output.message, "player");
                    break;

                case "update":
                    if(json.output.error != null) {
                        if(json.output.error == "Is not in game") {
                            newContent("The connection to server got dropped!", "error");
                            newContent("You have been disconnected from the server!", "server");
                            helpStage = 1;
                            game_id = null;
                            server_id = null;
                        }
                    }

                    if(json.output.chat != null) {
                        // We got some new messages

                        for (index in json.output.chat.messages) {
                            newContent(json.output.chat.messages[index], "player");
                        }
                    }

                    if(json.output.game != null) {
                        if(!hasOpponent) {
                            // Opponent got connected
                            newContent("An opponent " + json.output.game.opponent.name + " got connected!", "server");
                            hasOpponent = true;
                            helpStage = 10;
                        } else {
                            newContent("Opponent got disconnected!", "server");
                            hasOpponent = false;
                            helpStage = 11;
                        }
                    }
                    break;

                case "emotes":
                    //emotes = json.output.emotes;
                    var displayEM = "";
                    for(index in json.output.emotes) {
                        emotesLook.push(json.output.emotes[index]["regex"]);
                        emotesReplace.push("<img src='" + json.output.emotes[index]["url"] + "' title='" + json.output.emotes[index]["regex"] + "'>");
                        displayEM += emotesReplace[index];
                    }
                    newContent("Loaded " + emotesLook.length + " emotes!", "success");
                    newContent(displayEM);
                    break;

                case "turn":
                    if(json.output.data == 1) {
                        newContent("It's your turn!", "success");
                    } else {
                        newContent("It's opponent's turn!", "success");
                    }
                    break;

                case "factions_list":
                    var factions = json.output.factions;
                    for(index in factions) {
                        newContent("[" + index + "] " + factions[index], "message");
                    }
                    newContent("<b>[identifier] Faction | Faction List</b>", "message");
                    break;

                default:
                    console.log("Unknown command but here: ");
                    console.log(json);
                    break;
            }
        }
    };

    function connect(GameID) {
        queueRefreshAutoDestroy = true;
        newContent("Connecting...", "server");
        sendToServer("/connect " + GameID, handlers.command);
    }

    var postOutput = new Array();
    var outputCallback = null;

    function sendToServer(input, handler) {
        outputCallback = handler;
        console.log("Requesting " + input + " to the server");
        $.post("server_gate_simulator.ajax.php", {input: input, session_key: game_sessionkey}, function(data) {
            console.log("Fetched data: " + data);

            postOutput.push(data);
        }, "html");
    }


    function newContent(content, special = "") {
        if(content.length == 0) {
            console.error("Failed to print out new content to the console, empty content [" + content + "]");
            return;
        }

        var newC = "<div class='output " + special + " " + bruteforcestyle + "'>(" + new Date().toLocaleTimeString() + ") " + content + "</div>";
        console.log("Printing out the new output " + newC);
        $("#console_window").html( newC + $("#console_window").html() );

        bruteforcestyle = "";
    }



    function refresh() {
        if(outputCallback != null) {
            if(postOutput != null) {
                postOutput.forEach(function(item) {outputCallback(item);});

                postOutput = new Array();
                // outputCallback = null;
            }
        }

        if(game_id != null) {
            sendToServer("/update", handlers.command);
        }

        setTimeout(refresh, refresher);
    }
    refresh();

    function isJSON(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    module.exports = function strtr (str, trFrom, trTo) {
        //  discuss at: http://locutus.io/php/strtr/
        // original by: Brett Zamir (http://brett-zamir.me)
        //    input by: uestla
        //    input by: Alan C
        //    input by: Taras Bogach
        //    input by: jpfle
        // bugfixed by: Kevin van Zonneveld (http://kvz.io)
        // bugfixed by: Kevin van Zonneveld (http://kvz.io)
        // bugfixed by: Brett Zamir (http://brett-zamir.me)
        // bugfixed by: Brett Zamir (http://brett-zamir.me)
        //   example 1: var $trans = {'hello' : 'hi', 'hi' : 'hello'}
        //   example 1: strtr('hi all, I said hello', $trans)
        //   returns 1: 'hello all, I said hi'
        //   example 2: strtr('äaabaåccasdeöoo', 'äåö','aao')
        //   returns 2: 'aaabaaccasdeooo'
        //   example 3: strtr('ääääääää', 'ä', 'a')
        //   returns 3: 'aaaaaaaa'
        //   example 4: strtr('http', 'pthxyz','xyzpth')
        //   returns 4: 'zyyx'
        //   example 5: strtr('zyyx', 'pthxyz','xyzpth')
        //   returns 5: 'http'
        //   example 6: strtr('aa', {'a':1,'aa':2})
        //   returns 6: '2'

        var krsort = require('../array/krsort')
        var iniSet = require('../info/ini_set')

        var fr = ''
        var i = 0
        var j = 0
        var lenStr = 0
        var lenFrom = 0
        var sortByReference = false
        var fromTypeStr = ''
        var toTypeStr = ''
        var istr = ''
        var tmpFrom = []
        var tmpTo = []
        var ret = ''
        var match = false

        // Received replace_pairs?
        // Convert to normal trFrom->trTo chars
        if (typeof trFrom === 'object') {
            // Not thread-safe; temporarily set to true
            // @todo: Don't rely on ini here, use internal krsort instead
            sortByReference = iniSet('locutus.sortByReference', false)
            trFrom = krsort(trFrom)
            iniSet('locutus.sortByReference', sortByReference)

            for (fr in trFrom) {
                if (trFrom.hasOwnProperty(fr)) {
                    tmpFrom.push(fr)
                    tmpTo.push(trFrom[fr])
                }
            }

            trFrom = tmpFrom
            trTo = tmpTo
        }

        // Walk through subject and replace chars when needed
        lenStr = str.length
        lenFrom = trFrom.length
        fromTypeStr = typeof trFrom === 'string'
        toTypeStr = typeof trTo === 'string'

        for (i = 0; i < lenStr; i++) {
            match = false
            if (fromTypeStr) {
                istr = str.charAt(i)
                for (j = 0; j < lenFrom; j++) {
                    if (istr === trFrom.charAt(j)) {
                        match = true
                        break
                    }
                }
            } else {
                for (j = 0; j < lenFrom; j++) {
                    if (str.substr(i, trFrom[j].length) === trFrom[j]) {
                        match = true
                        // Fast forward
                        i = (i + trFrom[j].length) - 1
                        break
                    }
                }
            }
            if (match) {
                ret += toTypeStr ? trTo.charAt(j) : trTo[j]
            } else {
                ret += str.charAt(i)
            }
        }

        return ret
    }

</script>

<style>
    #console {
        background-color: black;
        color: white;
        font-family: "Courier New", Courier, monospace;
        padding-left: 2%;

        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    #console_window {
        width: 96%;
        height: 80%;
        position: fixed;
        top: 2%;
        left: 2%;
        overflow-y: scroll;
    }

    #console_input {
        border: 0;
        color: white;
        background-color: #111111;
        border-bottom: 1px dotted white;
        font-family: "Courier New", Courier, monospace;
        width: 96%;
        bottom: 2%;
        left: 2%;
        position: fixed;
    }

    .error {
        color: red;
    }

    .success {
        color: green;
    }

    .warning {
        color: orange;
    }

    .hint {
        color: coral;
    }

    .server {
        color: #7075d9;
    }

    .undefined {
        color: gray;
    }

    .message {
        color: #add5b3;
    }

    .player {
        color: #d58a53;
        font-family: Arial;
    }
</style>