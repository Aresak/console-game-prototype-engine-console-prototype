<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 14.05.2016
 * Time: 17:53
 */

// all is contained here
// Error List:
// #1 - Post hasn't been sent
// #2 - Request Type hasn't been sent
// #3 - Account hasn't been sent

if(!isset($_POST)) die("Error #1");

// include and establish database connection now
define("allowed_to_view_database_info", true);
include_once "../../../database.php";
include_once "../../../Cryospark.php";

if(!isset($_POST["RequestType"])) die("Error #2");
if(!isset($_POST["session_key"])) die("Error #3");

$requestType = $_POST["RequestType"];

// prepare the connection
$con = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
    or die(mysqli_error($con));


/*
 *
 * Requests:
 * GetPossibleFactions
 * CanChooseFaction
 * ChooseFaction
 * BuildOnTile
 * GetGameInfo
 * IamPregameReady
 * AreWePregameReady
 * GetCards
 * WhosTurn
 *
 */
// init Account, Game, Server
$account = new \BFT\Account();
$game = new \BFT\Game();
$server = new \BFT\Server();
if(!$account->loadObject($con, "game_sessionkey", $sess)) {
    // is not logged in
    $output["output"]["error"] = "Not logged in";
}

if(!$game->loadObject($con, "player1_id", $account->ID)) {
    if(!$game->loadObject($con, "player2_id", $account->ID)) {
        // is not in game
        $output["output"]["error"] = "Is not in game";
    } else $assignedSlot = 2;
} else $assignedSlot = 1;

if(!isset($assignedSlot)) die(json_encode(array("output" => array("error" => "Is not in game"))));

if(!$server->loadObject($con, "game_id", $game->ID)) {
    $output["output"]["error"] = "Server is not valid";
}

$output["output"]["RequestType"] = $requestType;

if(isset($output["output"]["error"])) die(json_encode($output));

switch($requestType) {
    // Lobby Requests Phase
    // Pregame Requests Phase
    case "GetPossibleFactions":
        // Return list of possible factions
        break;

    case "CanChooseFaction":
        // Can we choose a faction?
        break;

    case "ChooseFaction":
        // We want to choose faction
        break;

    // Game Phase
    case "BuildOnTile":
        // We want to build a structure or train a unit on tile
        break;

    case "GetGameInfo":
        // We want to know some info about the game ex. golds, turns, rounds ect.
        break;

    case "IamPregameReady":
        // I'm pregame ready
        break;

    case "AreWePregameReady":
        // Are we all pregame ready?
        break;

    case "GetCards":
        // Get List of my cards
        break;

    case "WhosTurn":
        // Whos playing right now?
        break;

    case "MoveUnit":
        // Want to move unit from tile x to tile y
        break;

    case "Interact":
        // Any kind of interaction ie. use of card, artifact act.
        break;

    case "GetTimes":
        // Get all possible info about times
        break;

    case "ServerMapInfo":
        // Get tiles info from the server for sync
        break;

    case "GetUnits":
        // Get info about all units on the map
        break;

    case "GetUpdates":
        // Get Updates for our requests
        break;
}

die(json_encode($output));