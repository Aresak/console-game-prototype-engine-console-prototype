<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 29.05.2016
 * Time: 17:39
 */
ini_set('display_errors', 'On');
error_reporting(E_ALL);


define("allowed_to_view_database_info", true);
include "../../../database.php";
include "../../../Cryospark.php";

// prepare the connection
$con = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
or die(mysqli_error($con));

class output
{
    public function error($msg)
    {
        die("<span class='ouput error'>@Server: $msg</span>");
    }

    public function server($msg)
    {
        echo "<span class='ouput server'>@Server: $msg</span>";
    }

    public function warning($msg)
    {
        echo "<span class='ouput warning'>@Server: $msg</span>";
    }

    public function success($msg)
    {
        echo "<span class='ouput success'>@Server: $msg</span>";
    }
}

$o = new output;
if (isset($_POST["input"])) {
    if (isset($_POST["session_key"])) {
        $input = $_POST["input"];
        $sess = $_POST["session_key"];
        $sessPERM = $sess;

        $processedInput = explode(" ", $input);

        if ($processedInput[0] == "/login") {
            $sess = generate64();

            $account = new \BFT\Account();

            if (!$account->loadObject($con, "ID", $processedInput[1])) {
                if (!$account->loadObject($con, "username", $processedInput[1])) {
                    $o->error("Couldn't login with neither ID or username");
                }
            }

            if ($account->game_sessionkey == $sessPERM && $sessPERM != null) $o->error("You are already logged in!");

            $account->updateObject($con, "game_sessionkey", $sess);
            $account->updateObject($con, "last_logged_game", time());

            $output = array();
            $output["output"]["command"] = "login";
            $output["output"]["data"] = $account->array;
            die(json_encode($output));
        } else if ($processedInput[0] == "/connect") {
            // connects to the server

            $account = new \BFT\Account();
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                $o->error("Account login is not valid!");
            }

            $game = new \BFT\Game();
            if (!$game->loadObject($con, "game_id", $processedInput[1])) {
                $o->error("Game " . $processedInput[1] . " not found!");
            }

            $server = new \BFT\Server();
            if (!$server->loadObject($con, "game_id", $processedInput[1])) {
                $o->error("Server with assigned game " . $processedInput[1] . " not found!");
            }

            if (!empty($server->password)) {
                if (!isset($processedInput[2])) $o->error("This server is locked on password!");

                if ($server->password != $processedInput[2]) {
                    $o->error("Server - Client password mismatch! The server is locked!");
                }
            }

            if ($server->locked == 1) $o->error("This server is locked!");

            $assignedSlot = 0;
            if ($game->player1_id == 0) {
                // Player 1 Slot is empty!
                $assignedSlot = 1;
                $game->updateObject($con, "player1_id", $account->ID);
                $game->updateObject($con, "player1_ping", time());
            } else if ($game->player2_id == 0) {
                // Player 2 Slot is empty!
                $assignedSlot = 2;
                $game->updateObject($con, "player2_id", $account->ID);
                $game->updateObject($con, "player2_ping", time());
            } else {
                $o->error("The server you tried to connect in is full!");
            }
            $server->updateObject($con, "players", $server->players + 1);
            $queue = new \BFT\Queue();
            $queue->loadObject($con, "client_id", $account->ID);
            $queue->deleteObject($con);

            $output = array();
            $output["output"]["command"] = "connect";
            $output["output"]["game"] = $game->array;

            die(json_encode($output));

        } else if ($processedInput[0] == "/disconnect") {
            // disconnects from the server

            $account = new \BFT\Account();
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                $o->error("Account login is not valid!");
            }

            $game = new \BFT\Game();
            $playerSlot = 0;
            if ($game->loadObject($con, "player1_id", $account->ID)) {
                $playerSlot = 1;
            } else if ($game->loadObject($con, "player2_id", $account->ID)) {
                $playerSlot = 2;
            } else {
                $o->error("Game for " . $account->display_name . " not found!");
            }

            $server = new \BFT\Server();
            if (!$server->loadObject($con, "game_id", $game->game_id)) {
                $o->error("Server for " . $account->display_name . " not found! (" . $game->game_id . ")");
            }

            // all's good
            $server->updateObject($con, "players", $server->players - 1);
            $game->updateObject($con, "player" . $playerSlot . "_id", 0);
            $output = array();
            $output["output"]["command"] = "disconnect";
            die(json_encode($output));
        } else if ($processedInput[0] == "/queue") {
            $account = new \BFT\Account();
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                $o->error("Account login is not valid!");
            }


            if (!isset($processedInput[1])) $o->error("You haven't parsed second argument! (/queue [join/leave/details]");
            if ($processedInput[1] == "join") {
                // Check if am I already in the queue
                $queue = new \BFT\Queue();
                if ($queue->loadObject($con, "client_id", $account->ID)) {
                    $o->error("You are already in the queue! Leave it first");
                }

                // joins the queue
                $joinQueueQuery = "INSERT INTO bft_ws1_queue (client_id, jointime, ping)
                VALUES ('" . $account->ID . "', '" . time() . "', '" . time() . "')";
                $joinQueueResult = mysqli_query($con, $joinQueueQuery)
                or die(mysqli_error($con));

                $output = array();
                $output["output"]["command"] = "queue_join";
                $myQueue = new \BFT\Queue();
                $myQueue->loadObject($con, "client_id", $account->ID);
                $output["output"]["data"] = $myQueue->array;

                die(json_encode($output));
            } else if ($processedInput[1] == "leave") {
                // leaves the queue
                // first check if there is the player in the queue
                $output = array();
                $output["output"]["command"] = "queue_leave";
                $queue = new \BFT\Queue();
                if (!$queue->loadObject($con, "client_id", $account->ID)) {
                    $o->error("You couldn't leave a queue, because you are not in any!");
                }


                $leaveQueueQuery = "DELETE FROM bft_ws1_queue WHERE client_id='" . $account->ID . "'";
                $leaveQueueResult = mysqli_query($con, $leaveQueueQuery)
                or die(mysqli_error($con));

                $output["output"]["data"]["leave_time"] = time();

                die(json_encode($output));
            } else if ($processedInput[1] == "details") {
                // displays info of the current queue

                $queuesQuery = "SELECT * FROM bft_ws1_queue";
                $queuesResult = mysqli_query($con, $queuesQuery)
                or die(mysqli_error($con));

                $waiting = 0;
                $selecting = 0;
                $waitingtime = 0;
                $result = new Result($queuesResult);
                for ($i = 0; $i < mysqli_num_rows($queuesResult); $i++) {
                    $g = $result->get("game", $i);
                    if(empty($g)) $waiting ++;
                    else $selecting ++;

                    unset($g);

                    $wt = $result->get("jointime", $i);
                    if ((time() - $wt) > $waitingtime) $waitingtime = time() - $wt;
                }

                $output = array();
                $output["output"]["command"] = "queue_details";
                $output["output"]["data"]["total"] = mysqli_num_rows($queuesResult);
                $output["output"]["data"]["waiting"] = $waiting;
                $output["output"]["data"]["selecting"] = $selecting;
                $output["output"]["data"]["waitingtime"] = $waitingtime;

                die(json_encode($output));
            } else if ($processedInput[1] == "refresh") {
                $queue = new \BFT\Queue();
                if (!$queue->loadObject($con, "client_id", $account->ID)) {
                    $o->error("Client is not in the queue!");
                }

                $output = array();
                $output["output"]["command"] = "queue_refresh";
                $queue->updateObject($con, "ping", time());
                $output["output"]["data"] = $queue->array;
                die(json_encode($output));
            } else {
                $o->error("You have entered invalid second parameter! Valid: (/queue [join/leave/details]");
            }
        } else if ($processedInput[0] == "/opponent") {
            if ($processedInput[1] == "details") {
                $account = new \BFT\Account();
                if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                    $o->error("Account login is not valid!");
                }

                $game = new \BFT\Game();
                if (!$game->loadObject($con, "player1_id", $account->ID)) {
                    if (!$game->loadObject($con, "player2_id", $account->ID)) {
                        $o->error("You are not in any game!");
                    } else $assignedSlot = 2;
                } else $assignedSlot = 1;


                $opponent = new \BFT\Account(); // opponent's accounts
                if ($assignedSlot == 1) {
                    $opponent->loadObject($con, "ID", $game->player2_id);
                } else {
                    $opponent->loadObject($con, "ID", $game->player1_id);
                }

                if ($opponent->failed) {
                    $o->error("You don't have opponent yet!");
                }

                $o->server("Your opponent is " . $opponent->display_name);
            }
        } else if ($processedInput[0] == "/faction") {
            $account = new \BFT\Account();
            $output = array();
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                // is not logged in
                $output["output"]["error"] = "Not logged in";
            }

            // Processing function
            $game = new \BFT\Game();
            $server = new \BFT\Server();
            if (!$game->loadObject($con, "player1_id", $account->ID)) {
                if (!$game->loadObject($con, "player2_id", $account->ID)) {
                    // is not in game
                    $output["output"]["error"] = "Is not in game";
                } else $assignedSlot = 2;
            } else $assignedSlot = 1;

            if (!isset($assignedSlot)) die(json_encode(array("output" => array("error" => "Is not in game"))));

            $server->loadObject($con, "game_id", $game->game_id);
            $game->updateObject($con, "player" . $assignedSlot . "_ping", time());

            if ($processedInput[1] == "select") {
                $output["output"]["command"] = "faction_select";
                if ($processedInput[2] == "random") {
                    // select random faction

                } else {
                    // select faction $processedInput[3]

                }
            } else if ($processedInput[1] == "list") {
                // Get the list of factions
                $output["output"]["command"] = "faction_list";

                echo "Hi";
                // syntax:
                // $output[output][factions][identifier] = en lang name


            }

            die(json_encode($output));
        } else if ($processedInput[0] == "/status") {
            $account = new \BFT\Account();
            $output = array();
            $output["output"]["command"] = "status";
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                // is not logged in
                $output["output"]["status"] = 1;
            }

            $game = new \BFT\Game();
            if (!$game->loadObject($con, "player1_id", $account->ID)) {
                if (!$game->loadObject($con, "player2_id", $account->ID)) {
                    // is not in game
                    $output["output"]["status"] = 2;
                } else $assignedSlot = 2;
            } else $assignedSlot = 1;

            $game->updateObject($con, "player" . $assignedSlot . "_ping", time());


            $opponent = new \BFT\Account(); // opponent's accounts
            if ($assignedSlot == 1) {
                $opponent->loadObject($con, "ID", $game->player2_id);
            } else {
                $opponent->loadObject($con, "ID", $game->player1_id);
            }

            if ($opponent->failed) {
                // doesn't have opponent yet
                $output["output"]["status"] = 3;
            }

            $output["output"]["data"]["opponent"]["name"] = $opponent->display_name;

            // ...


            die(json_encode($output));
        } else if ($processedInput[0] == "msg") {
            $account = new \BFT\Account();
            $game = new \BFT\Game();
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                // is not logged in
                $output["output"]["error"] = "Not logged in";
                $o->error("Not logged in!");
            }

            if (!$game->loadObject($con, "player1_id", $account->ID)) {
                if (!$game->loadObject($con, "player2_id", $account->ID)) {
                    // is not in game
                    $output["output"]["error"] = "You are not in the game yet!";
                    $o->error("You are not in the game yet!");
                } else $assignedSlot = 2;
            } else $assignedSlot = 1;

            if (!isset($assignedSlot)) $o->error("You are not in the game yet!");

            $game->updateObject($con, "player" . $assignedSlot . "_ping", time());

            $readers = array($account->ID);
            $readers = json_encode($readers);

            $message = "@" . $account->display_name . ": ";
            for ($i = 1; $i < count($processedInput); $i++) {
                $message .= $processedInput[$i] . " ";
            }

            $emotesQuery = "SELECT * FROM bft_emoticons";
            $emotesResult = mysqli_query($con, $emotesQuery)
            or die(mysqli_error($con));

            $emRes = new Result($emotesResult);

            $emL = array("");
            $emR = array("");
            for ($i = 0; $i < mysqli_num_rows($emotesResult); $i++) {
                $emL[$i] = $emRes->get("regex", $i);
                $emR[$i] = "<img src=" . $emRes->get("url", $i) . ">";
            }

            $message = str_replace($emL, $emR, $message);


            $message = mysqli_escape_string($con, $message);
            $messageQuery = "INSERT INTO bft_ws1_chat (lobby, message, data, time) VALUES ('#game" . $game->game_id . "', '$message', '$readers', '" . time() . "')";
            $messageResult = mysqli_query($con, $messageQuery)
            or die(mysqli_error($con));
            $output["output"]["command"] = "message";
            $output["output"]["message"] = $message;
            die(json_encode($output));
        } else if ($processedInput[0] == "/update") {
            $account = new \BFT\Account();
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                // is not logged in
                $output["output"]["error"] = "Not logged in";
            }

            // Processing function
            $game = new \BFT\Game();
            $server = new \BFT\Server();
            if (!$game->loadObject($con, "player1_id", $account->ID)) {
                if (!$game->loadObject($con, "player2_id", $account->ID)) {
                    // is not in game
                    $output["output"]["error"] = "Is not in game";
                } else $assignedSlot = 2;
            } else $assignedSlot = 1;

            if (!isset($assignedSlot)) die(json_encode(array("output" => array("error" => "Is not in game"))));

            $server->loadObject($con, "game_id", $game->game_id);
            $game->updateObject($con, "player" . $assignedSlot . "_ping", time());

            if($server->players == 2) {
                $output["output"]["game"]["round"]  = $game->round;
                $output["output"]["game"]["turn"]   = $game->turn;
                $output["output"]["game"]["turns"]  = $game->turn_total;
                $output["output"]["game"]["stage"]  = $game->stage;
                $output["output"]["game"]["timer"]  = $game->timer;
                $output["output"]["game"]["sync"]   = $game->synctime;

                $opponent = new \BFT\Account();
                $opponent->loadObject($con, "ID", ($assignedSlot == 1 ? $game->player2_id : $game->player1_id));

                $output["output"]["game"]["opponent"]["name"] = $opponent->display_name;

                if($assignedSlot == 1)  $output["output"]["game"]["opponent_ping"]  = $game->player2_ping;
                else                    $output["output"]["game"]["opponent_ping"]  = $game->player1_ping;
            }

            // Fetch Chat Messages
            $chatQuery = "SELECT * FROM bft_ws1_chat WHERE lobby='#game" . $game->game_id . "'";
            $chatResult = mysqli_query($con, $chatQuery)
            or die(mysqli_error($con));

            $output = array();
            $output["output"]["command"] = "update";

            $result = new Result($chatResult);

            for ($i = 0; $i < mysqli_num_rows($chatResult); $i++) {
                // Each new message
                $data = $result->get("data", $i); // Check if we have read it

                $read = false;
                try {
                    $dataJSON = json_decode($data);
                    foreach ($dataJSON as $acc) {
                        if ($acc == $account->ID) {
                            $read = true;
                            break;
                        }
                    }
                } catch (Exception $i) {
                    $read = false;
                } // We haven't read it

                if (!$read) {
                    // we haven't read it yet
                    $output["output"]["chat"]["messages"][$i] = $result->get("message", $i);
                    $chatMessage = new \BFT\Chat();
                    $chatMessage->loadObject($con, "ID", $result->get("ID", $i));
                    $newReaders = $dataJSON;
                    array_push($newReaders, $account->ID);
                    $newReaders = json_encode($newReaders);
                    $chatMessage->updateObject($con, "data", $newReaders);
                }
            }
            die(json_encode($output));
        } else if ($processedInput[0] == "/emotes") {
            $emotesQuery = "SELECT * FROM bft_emoticons";
            $emotesResult = mysqli_query($con, $emotesQuery)
            or die(mysqli_error($con));

            $output["output"]["command"] = "emotes";
            $output["output"]["emotes"] = array();

            $result = new Result($emotesResult);
            for ($x = 0; $x < mysqli_num_rows($emotesResult); $x++) {
                $output["output"]["emotes"][$x]["regex"] = $result->get("regex", $x);
                $output["output"]["emotes"][$x]["width"] = $result->get("width", $x);
                $output["output"]["emotes"][$x]["height"] = $result->get("height", $x);
                $output["output"]["emotes"][$x]["url"] = $result->get("url", $x);
                $output["output"]["emotes"][$x]["emoticon_set"] = $result->get("emoticon_set", $x);
            }

            die(json_encode($output));
        } else if ($processedInput[0] == "/status") {
            $account = new \BFT\Account();
            $output["output"]["command"] = "status";
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                // is not logged in
                $output["output"]["error"] = "Not logged in";
            }

            // Processing function
            $game = new \BFT\Game();
            $server = new \BFT\Server();
            if (!$game->loadObject($con, "player1_id", $account->ID)) {
                if (!$game->loadObject($con, "player2_id", $account->ID)) {
                    // is not in game
                    $output["output"]["error"] = "Is not in game";
                } else $assignedSlot = 2;
            } else $assignedSlot = 1;

            if (!isset($assignedSlot)) die(json_encode(array("output" => array("error" => "Is not in game"))));
            if ($game->stage == 0) {
                $output["output"]["status"] = "Waiting for opponent";
            }
            else if($game->stage == 1) {
                $output["output"]["status"] = "Pre-Game";
            }
            else if($game->stage == 2) {
                $output["output"]["status"] = "In Game";
            }
            else if($game->stage == 3) {
                $output["output"]["status"] = "Announcement";
            }

            die(json_encode($output));
        }
        else if($processedInput[0] == "/turn") {
            $account = new \BFT\Account();
            if (!$account->loadObject($con, "game_sessionkey", $sess)) {
                // is not logged in
                $output["output"]["error"] = "Not logged in";
            }

            // Processing function
            $game = new \BFT\Game();
            $server = new \BFT\Server();
            if (!$game->loadObject($con, "player1_id", $account->ID)) {
                if (!$game->loadObject($con, "player2_id", $account->ID)) {
                    // is not in game
                    $output["output"]["error"] = "Is not in game";
                } else $assignedSlot = 2;
            } else $assignedSlot = 1;

            if (!isset($assignedSlot)) die(json_encode(array("output" => array("error" => "Is not in game"))));

            $server->loadObject($con, "game_id", $game->game_id);
            $game->updateObject($con, "player" . $assignedSlot . "_ping", time());

            $output["output"]["command"] = "turn";
            $output["output"]["data"] = ($game->turn == $account->ID ? 1 : 0);

            die(json_encode($output));
        } else var_dump($processedInput);
    } else $o->error("Game Session Key haven't been sent");
} else $o->error("Input post message haven't been sent");
