<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 09.06.2016
 * Time: 11:24
 */

namespace Factions;


/**
 * Class Faction
 * @package Factions
 */
class Faction {
    public $identifier;
    public $lang;
    public $units;

    function __construct($identifier, $enEN = "", $csCZ = "", $units)
    {
        $this->identifier = $identifier;
        $this->lang["en-EN"] = $enEN;
        $this->lang["cs-CZ"] = $csCZ;
        $this->units = $units;
    }

    public function toArray() {
        $result = array();
        $result["identifier"]   = $this->identifier;
        $result["lang"]["en-EN"]= $this->lang["en-EN"];
        $result["lang"]["cs-CZ"]= $this->lang["cs-CZ"];
        $result["units"]        = array();
        foreach($this->units as $unit) {
            $result["units"][$unit->identifier] = $unit->toArray();
        }

        return $result;
    }
}

class Unit {
    public $entityType, $identifier;
    public $lang;

    public $price, $damage, $range, $health, $defense, $resistance, $speed, $population;

    function __construct($identifier, $enEN = "", $csCZ = "")
    {
        $this->identifier = $identifier;
        $this->lang["en-EN"] = $enEN;
        $this->lang["cs-CZ"] = $csCZ;
    }

    public function toArray() {
        $result = array();
        $result["identifier"]               = $this->identifier;
        $result["entityType"]               = $this->entityType;
        $result["lang"]                     = $this->lang;
        $result["attributes"]["price"]      = $this->price;
        $result["attributes"]["damage"]     = $this->damage;
        $result["attributes"]["range"]      = $this->range;
        $result["attributes"]["health"]     = $this->health;
        $result["attributes"]["defense"]    = $this->defense;
        $result["attributes"]["resistance"] = $this->resistance;
        $result["attributes"]["speed"]      = $this->speed;
        $result["attributes"]["population"] = $this->population;

        return $result;
    }

    public function getOnKillGold() {
        return ($this->price / 100) * 20;
    }
}