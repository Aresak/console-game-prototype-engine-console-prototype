<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 21.06.2016
 * Time: 16:13
 */

namespace Map;


class Map {
    public $tiles;

    public function toArray() {
        $result = array();
        foreach($this->tiles as $tile) {
            $result[$tile->x][$tile->y] = $tile->toArray();
        }
        return $result;
    }

    // todo this function
    public static function generate($rules) {

    }
}

class Tile {
    public $x, $y;
    public $tileType, $occupied, $entity;

    public function toArray() {
        $result = array();
        $result["x"] = $this->x;
        $result["y"] = $this->y;
        $result["tileType"] = $this->tileType;
        $result["occupied"] = $this->occupied;
        $result["entity"]   = $this->entity;
        return $result;
    }
}

class TileType {
    public $void    = 0;
    public $team_a  = 1;
    public $team_b  = 2;
    public $treasure= 3;
    public $playground  = 4;
    public $monster = 5;
    public $walls_a = 6;
    public $tower_a = 7;
    public $gate_a  = 8;
    public $walls_b = 9;
    public $tower_b = 10;
    public $gate_b  = 11;
    public $pillars = 12;
    public $entrance= 13;
    public $shadows = 14;

    public static function resolveTile($tileTypeID) {
        $tt = new TileType();
        $array = (array) $tt;
        foreach($array as $i => $value) {
            if($tileTypeID == $array[$i]) return $i;
        }
        return false;
    }

    public static function getTileTypes() {
        $tt = new TileType();
        return (array) $tt;
    }
}