<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 09.06.2016
 * Time: 11:31
 */


require_once "Faction.php";

// Script to create the factions
// Ready to Import Script
// // RIS // //

// unit list
// 0 - leader
// 1 - soldier
// 2 - shooter
// 3 - siege
// 4 - support
// 5 - magician
// 6 - protector
// 7 - conqueror

// faction list
// 0 - Empire
// 1 - Elven Kingdom
// 2 - Tribe of Mountain Dwarves
// 3 - Horde of Orcs
// 4 - Undead Legion
// 5 - Barbarian Tribes



// Units Array
$empireUnits            = array();
$elvenKingdomUnits      = array();
$mountainDwarvesUnits   = array();
$hordeoforcsUnits       = array();
$undeadLegionUnits      = array();
$barbarianTribesUnits   = array();


// Units Placer
//          Empire                                                                                      Elven Kingdom
$empireUnits["leader"]              = new \Factions\Unit("emperor", "Emperor");                     $elvenKingdomUnits["leader"]        = new \Factions\Unit("elven_king", "Elven King");
$empireUnits["soldier"]             = new \Factions\Unit("squire", "Squire");                       $elvenKingdomUnits["soldier"]       = new \Factions\Unit("elven_lancer", "Elven Lancer");
$empireUnits["shooter"]             = new \Factions\Unit("hunter", "Hunter");                       $elvenKingdomUnits["shooter"]       = new \Factions\Unit("elver_ranger", "Elven Ranger");
$empireUnits["siege"]               = new \Factions\Unit("catapult", "Catapult");                   $elvenKingdomUnits["siege"]         = new \Factions\Unit("ballista", "Ballista");
$empireUnits["support"]             = new \Factions\Unit("standard_bearer", "Standard-bearer");     $elvenKingdomUnits["support"]       = new \Factions\Unit("dryad", "Dryad");
$empireUnits["magician"]            = new \Factions\Unit("healer", "Healer");                       $elvenKingdomUnits["magician"]      = new \Factions\Unit("druid", "Druid");
$empireUnits["protector"]           = new \Factions\Unit("knight", "Knight");                       $elvenKingdomUnits["protector"]     = new \Factions\Unit("mossy_guardian", "Mossy Guardian");
$empireUnits["conqueror"]           = new \Factions\Unit("cavalier", "Cavalier");                   $elvenKingdomUnits["conqueror"]     = new \Factions\Unit("ent", "Ent");

//          Tribe of Mountain Dwarves                                                                   Horde of Orcs
$mountainDwarvesUnits["leader"]     = new \Factions\Unit("chieftain", "Chieftain");                 $hordeoforcsUnits["leader"]         = new \Factions\Unit("horde_leader", "Horde Leader");
$mountainDwarvesUnits["soldier"]    = new \Factions\Unit("axeman", "Axeman");                       $hordeoforcsUnits["soldier"]        = new \Factions\Unit("goblin", "Goblin");
$mountainDwarvesUnits["shooter"]    = new \Factions\Unit("axe_thrower", "Axe Thrower");             $hordeoforcsUnits["shooter"]        = new \Factions\Unit("orc_crossbowman", "Orc Crossbowman");
$mountainDwarvesUnits["siege"]      = new \Factions\Unit("ram", "Ram");                             $hordeoforcsUnits["siege"]          = new \Factions\Unit("troll", "Troll");
$mountainDwarvesUnits["support"]    = new \Factions\Unit("alchemist", "Alchemist");                 $hordeoforcsUnits["support"]        = new \Factions\Unit("orc_bard", "Orc Bard");
$mountainDwarvesUnits["magician"]   = new \Factions\Unit("rune_mage", "Rune mage");                 $hordeoforcsUnits["magician"]       = new \Factions\Unit("orc_shaman", "Orc Shaman");
$mountainDwarvesUnits["protector"]  = new \Factions\Unit("elite_dwarf", "Elite Dwarf");             $hordeoforcsUnits["protector"]      = new \Factions\Unit("hog_rider", "Hog Rider");
$mountainDwarvesUnits["conqueror"]  = new \Factions\Unit("mountain_giant", "Mountain Giant");       $hordeoforcsUnits["conqueror"]      = new \Factions\Unit("ogre", "Ogre");

//          Undead Legion                                                                               Barbarian Tribes
$undeadLegionUnits["leader"]        = new \Factions\Unit("necromancer", "Necromancer");             $barbarianTribesUnits["leader"]     = new \Factions\Unit("seer", "Seer");
$undeadLegionUnits["soldier"]       = new \Factions\Unit("ghoul", "Ghoul");                         $barbarianTribesUnits["soldier"]    = new \Factions\Unit("berserker", "Berserker");
$undeadLegionUnits["shooter"]       = new \Factions\Unit("skeleton_archer", "Skeleton Archer");     $barbarianTribesUnits["shooter"]    = new \Factions\Unit("raider", "Raider");
$undeadLegionUnits["siege"]         = new \Factions\Unit("wurm", "Wurm");                           $barbarianTribesUnits["siege"]      = new \Factions\Unit("elder", "Elder");
$undeadLegionUnits["support"]       = new \Factions\Unit("banshee", "Banshee");                     $barbarianTribesUnits["support"]    = new \Factions\Unit("sling", "Sling");
$undeadLegionUnits["magician"]      = new \Factions\Unit("grim_reaper", "Grim Reaper");             $barbarianTribesUnits["magician"]   = new \Factions\Unit("crier", "Crier");
$undeadLegionUnits["protector"]     = new \Factions\Unit("abomination", "Abomination");             $barbarianTribesUnits["protector"]  = new \Factions\Unit("half_giant", "Half-giant");
$undeadLegionUnits["conqueror"]     = new \Factions\Unit("vampire", "Vampire");                     $barbarianTribesUnits["conqueror"]  = new \Factions\Unit("cyclops", "Cyclops");


// Units Attributes Set
// Empire
$empireUnits["leader"]->health     = 200;   $empireUnits["soldier"]->health     = 95;   $empireUnits["shooter"]->health     = 80;
$empireUnits["leader"]->damage     = 32;    $empireUnits["soldier"]->damage     = 28;   $empireUnits["shooter"]->damage     = 30;
$empireUnits["leader"]->defense    = 7;     $empireUnits["soldier"]->defense    = 7;    $empireUnits["shooter"]->defense    = 3;
$empireUnits["leader"]->resistance = 6;     $empireUnits["soldier"]->resistance = 5;    $empireUnits["shooter"]->resistance = 3;
$empireUnits["leader"]->speed      = 3;     $empireUnits["soldier"]->speed      = 3;    $empireUnits["shooter"]->speed      = 4;
$empireUnits["leader"]->range      = 1;     $empireUnits["soldier"]->range      = 1;    $empireUnits["shooter"]->range      = 5;
$empireUnits["leader"]->price      = 0;     $empireUnits["soldier"]->price      = 100;  $empireUnits["shooter"]->price      = 125;
$empireUnits["leader"]->population = 0;     $empireUnits["soldier"]->population = 2;    $empireUnits["shooter"]->population = 2;
$empireUnits["leader"]->entityType = 0;     $empireUnits["soldier"]->entityType = 1;    $empireUnits["shooter"]->entityType = 2;

$empireUnits["siege"]->health     = 180;      $empireUnits["support"]->health     = 90;    $empireUnits["magician"]->health     = 85;
$empireUnits["siege"]->damage     = 135;      $empireUnits["support"]->damage     = 26;    $empireUnits["magician"]->damage     = 30;
$empireUnits["siege"]->defense    = 4;      $empireUnits["support"]->defense    = 5;    $empireUnits["magician"]->defense    = 3;
$empireUnits["siege"]->resistance = 2;      $empireUnits["support"]->resistance = 4;    $empireUnits["magician"]->resistance = 9;
$empireUnits["siege"]->speed      = 2;      $empireUnits["support"]->speed      = 3;    $empireUnits["magician"]->speed      = 3;
$empireUnits["siege"]->range      = 10;      $empireUnits["support"]->range      = 2;    $empireUnits["magician"]->range      = 1;
$empireUnits["siege"]->price      = 150;      $empireUnits["support"]->price      = 200;    $empireUnits["magician"]->price      = 300;
$empireUnits["siege"]->population = 3;      $empireUnits["support"]->population = 3;    $empireUnits["magician"]->population = 4;
$empireUnits["siege"]->entityType = 3;      $empireUnits["support"]->entityType = 4;    $empireUnits["magician"]->entityType = 5;

                $empireUnits["protector"]->health     = 185;      $empireUnits["conqueror"]->health     = 280;
                $empireUnits["protector"]->damage     = 36;      $empireUnits["conqueror"]->damage     = 45;
                $empireUnits["protector"]->defense    = 12;      $empireUnits["conqueror"]->defense    = 11;
                $empireUnits["protector"]->resistance = 9;      $empireUnits["conqueror"]->resistance = 9;
                $empireUnits["protector"]->speed      = 3;      $empireUnits["conqueror"]->speed      = 7;
                $empireUnits["protector"]->range      = 1;      $empireUnits["conqueror"]->range      = 1;
                $empireUnits["protector"]->price      = 300;      $empireUnits["conqueror"]->price      = 400;
                $empireUnits["protector"]->population = 4;      $empireUnits["conqueror"]->population = 2;
                $empireUnits["protector"]->entityType = 6;      $empireUnits["conqueror"]->entityType = 7;

// Elven Kingdom
$elvenKingdomUnits["leader"]->health     = 185;   $elvenKingdomUnits["soldier"]->health     = 90;   $elvenKingdomUnits["shooter"]->health     = 76;
$elvenKingdomUnits["leader"]->damage     = 30;    $elvenKingdomUnits["soldier"]->damage     = 30;   $elvenKingdomUnits["shooter"]->damage     = 33;
$elvenKingdomUnits["leader"]->defense    = 6;     $elvenKingdomUnits["soldier"]->defense    = 6;    $elvenKingdomUnits["shooter"]->defense    = 4;
$elvenKingdomUnits["leader"]->resistance = 7;     $elvenKingdomUnits["soldier"]->resistance = 4;    $elvenKingdomUnits["shooter"]->resistance = 3;
$elvenKingdomUnits["leader"]->speed      = 4;     $elvenKingdomUnits["soldier"]->speed      = 4;    $elvenKingdomUnits["shooter"]->speed      = 5;
$elvenKingdomUnits["leader"]->range      = 1;     $elvenKingdomUnits["soldier"]->range      = 2;    $elvenKingdomUnits["shooter"]->range      = 7;
$elvenKingdomUnits["leader"]->price      = 0;     $elvenKingdomUnits["soldier"]->price      = 100;  $elvenKingdomUnits["shooter"]->price      = 125;
$elvenKingdomUnits["leader"]->population = 0;     $elvenKingdomUnits["soldier"]->population = 2;    $elvenKingdomUnits["shooter"]->population = 2;
$elvenKingdomUnits["leader"]->entityType = 0;     $elvenKingdomUnits["soldier"]->entityType = 1;    $elvenKingdomUnits["shooter"]->entityType = 2;

$elvenKingdomUnits["siege"]->health     = 170;      $elvenKingdomUnits["support"]->health     = 72;    $elvenKingdomUnits["magician"]->health     = 100;
$elvenKingdomUnits["siege"]->damage     = 115;      $elvenKingdomUnits["support"]->damage     = 27;    $elvenKingdomUnits["magician"]->damage     = 32;
$elvenKingdomUnits["siege"]->defense    = 5;      $elvenKingdomUnits["support"]->defense    = 3;    $elvenKingdomUnits["magician"]->defense    = 5;
$elvenKingdomUnits["siege"]->resistance = 3;      $elvenKingdomUnits["support"]->resistance = 4;    $elvenKingdomUnits["magician"]->resistance = 5;
$elvenKingdomUnits["siege"]->speed      = 3;      $elvenKingdomUnits["support"]->speed      = 4;    $elvenKingdomUnits["magician"]->speed      = 3;
$elvenKingdomUnits["siege"]->range      = 11;      $elvenKingdomUnits["support"]->range      = 4;    $elvenKingdomUnits["magician"]->range      = 4;
$elvenKingdomUnits["siege"]->price      = 150;      $elvenKingdomUnits["support"]->price      = 200;    $elvenKingdomUnits["magician"]->price      = 225;
$elvenKingdomUnits["siege"]->population = 3;      $elvenKingdomUnits["support"]->population = 3;    $elvenKingdomUnits["magician"]->population = 3;
$elvenKingdomUnits["siege"]->entityType = 3;      $elvenKingdomUnits["support"]->entityType = 4;    $elvenKingdomUnits["magician"]->entityType = 5;

                        $elvenKingdomUnits["protector"]->health     = 180;      $elvenKingdomUnits["conqueror"]->health     = 320;
                        $elvenKingdomUnits["protector"]->damage     = 35;      $elvenKingdomUnits["conqueror"]->damage     = 48;
                        $elvenKingdomUnits["protector"]->defense    = 10;      $elvenKingdomUnits["conqueror"]->defense    = 9;
                        $elvenKingdomUnits["protector"]->resistance = 8;      $elvenKingdomUnits["conqueror"]->resistance = 6;
                        $elvenKingdomUnits["protector"]->speed      = 3;      $elvenKingdomUnits["conqueror"]->speed      = 4;
                        $elvenKingdomUnits["protector"]->range      = 1;      $elvenKingdomUnits["conqueror"]->range      = 2;
                        $elvenKingdomUnits["protector"]->price      = 300;      $elvenKingdomUnits["conqueror"]->price      = 400;
                        $elvenKingdomUnits["protector"]->population = 4;      $elvenKingdomUnits["conqueror"]->population = 2;
                        $elvenKingdomUnits["protector"]->entityType = 6;      $elvenKingdomUnits["conqueror"]->entityType = 7;

// Tribe of Mountain Dwarves
$mountainDwarvesUnits["leader"]->health     = 220;   $mountainDwarvesUnits["soldier"]->health     = 100;   $mountainDwarvesUnits["shooter"]->health     = 85;
$mountainDwarvesUnits["leader"]->damage     = 34;    $mountainDwarvesUnits["soldier"]->damage     = 32;   $mountainDwarvesUnits["shooter"]->damage     = 29;
$mountainDwarvesUnits["leader"]->defense    = 8;     $mountainDwarvesUnits["soldier"]->defense    = 5;    $mountainDwarvesUnits["shooter"]->defense    = 4;
$mountainDwarvesUnits["leader"]->resistance = 4;     $mountainDwarvesUnits["soldier"]->resistance = 3;    $mountainDwarvesUnits["shooter"]->resistance = 3;
$mountainDwarvesUnits["leader"]->speed      = 4;     $mountainDwarvesUnits["soldier"]->speed      = 3;    $mountainDwarvesUnits["shooter"]->speed      = 4;
$mountainDwarvesUnits["leader"]->range      = 1;     $mountainDwarvesUnits["soldier"]->range      = 1;    $mountainDwarvesUnits["shooter"]->range      = 5;
$mountainDwarvesUnits["leader"]->price      = 0;     $mountainDwarvesUnits["soldier"]->price      = 100;  $mountainDwarvesUnits["shooter"]->price      = 125;
$mountainDwarvesUnits["leader"]->population = 0;     $mountainDwarvesUnits["soldier"]->population = 2;    $mountainDwarvesUnits["shooter"]->population = 2;
$mountainDwarvesUnits["leader"]->entityType = 0;     $mountainDwarvesUnits["soldier"]->entityType = 1;    $mountainDwarvesUnits["shooter"]->entityType = 2;

$mountainDwarvesUnits["siege"]->health     = 200;      $mountainDwarvesUnits["support"]->health     = 85;    $mountainDwarvesUnits["magician"]->health     = 90;
$mountainDwarvesUnits["siege"]->damage     = 105;      $mountainDwarvesUnits["support"]->damage     = 28;    $mountainDwarvesUnits["magician"]->damage     = 28;
$mountainDwarvesUnits["siege"]->defense    = 6;      $mountainDwarvesUnits["support"]->defense    = 3;    $mountainDwarvesUnits["magician"]->defense    = 4;
$mountainDwarvesUnits["siege"]->resistance = 5;      $mountainDwarvesUnits["support"]->resistance = 4;    $mountainDwarvesUnits["magician"]->resistance = 5;
$mountainDwarvesUnits["siege"]->speed      = 4;      $mountainDwarvesUnits["support"]->speed      = 3;    $mountainDwarvesUnits["magician"]->speed      = 3;
$mountainDwarvesUnits["siege"]->range      = 1;      $mountainDwarvesUnits["support"]->range      = 4;    $mountainDwarvesUnits["magician"]->range      = 4;
$mountainDwarvesUnits["siege"]->price      = 150;      $mountainDwarvesUnits["support"]->price      = 200;    $mountainDwarvesUnits["magician"]->price      = 225;
$mountainDwarvesUnits["siege"]->population = 3;      $mountainDwarvesUnits["support"]->population = 3;    $mountainDwarvesUnits["magician"]->population = 3;
$mountainDwarvesUnits["siege"]->entityType = 3;      $mountainDwarvesUnits["support"]->entityType = 4;    $mountainDwarvesUnits["magician"]->entityType = 5;

                        $mountainDwarvesUnits["protector"]->health     = 210;      $mountainDwarvesUnits["conqueror"]->health     = 300;
                        $mountainDwarvesUnits["protector"]->damage     = 36;      $mountainDwarvesUnits["conqueror"]->damage     = 50;
                        $mountainDwarvesUnits["protector"]->defense    = 8;      $mountainDwarvesUnits["conqueror"]->defense    = 9;
                        $mountainDwarvesUnits["protector"]->resistance = 7;      $mountainDwarvesUnits["conqueror"]->resistance = 5;
                        $mountainDwarvesUnits["protector"]->speed      = 4;      $mountainDwarvesUnits["conqueror"]->speed      = 5;
                        $mountainDwarvesUnits["protector"]->range      = 1;      $mountainDwarvesUnits["conqueror"]->range      = 2;
                        $mountainDwarvesUnits["protector"]->price      = 300;      $mountainDwarvesUnits["conqueror"]->price      = 400;
                        $mountainDwarvesUnits["protector"]->population = 4;      $mountainDwarvesUnits["conqueror"]->population = 2;
                        $mountainDwarvesUnits["protector"]->entityType = 6;      $mountainDwarvesUnits["conqueror"]->entityType = 7;

// Horde of Orcs
$hordeoforcsUnits["leader"]->health     = 210;   $hordeoforcsUnits["soldier"]->health     = 70;   $hordeoforcsUnits["shooter"]->health     = 74;
$hordeoforcsUnits["leader"]->damage     = 35;    $hordeoforcsUnits["soldier"]->damage     = 27;   $hordeoforcsUnits["shooter"]->damage     = 28;
$hordeoforcsUnits["leader"]->defense    = 5;     $hordeoforcsUnits["soldier"]->defense    = 4;    $hordeoforcsUnits["shooter"]->defense    = 3;
$hordeoforcsUnits["leader"]->resistance = 3;     $hordeoforcsUnits["soldier"]->resistance = 3;    $hordeoforcsUnits["shooter"]->resistance = 2;
$hordeoforcsUnits["leader"]->speed      = 4;     $hordeoforcsUnits["soldier"]->speed      = 4;    $hordeoforcsUnits["shooter"]->speed      = 3;
$hordeoforcsUnits["leader"]->range      = 1;     $hordeoforcsUnits["soldier"]->range      = 1;    $hordeoforcsUnits["shooter"]->range      = 6;
$hordeoforcsUnits["leader"]->price      = 0;     $hordeoforcsUnits["soldier"]->price      = 100;  $hordeoforcsUnits["shooter"]->price      = 125;
$hordeoforcsUnits["leader"]->population = 0;     $hordeoforcsUnits["soldier"]->population = 2;    $hordeoforcsUnits["shooter"]->population = 2;
$hordeoforcsUnits["leader"]->entityType = 0;     $hordeoforcsUnits["soldier"]->entityType = 1;    $hordeoforcsUnits["shooter"]->entityType = 2;

$hordeoforcsUnits["siege"]->health     = 210;      $hordeoforcsUnits["support"]->health     = 88;    $hordeoforcsUnits["magician"]->health     = 78;
$hordeoforcsUnits["siege"]->damage     = 100;      $hordeoforcsUnits["support"]->damage     = 27;    $hordeoforcsUnits["magician"]->damage     = 29;
$hordeoforcsUnits["siege"]->defense    = 4;      $hordeoforcsUnits["support"]->defense    = 4;    $hordeoforcsUnits["magician"]->defense    = 3;
$hordeoforcsUnits["siege"]->resistance = 2;      $hordeoforcsUnits["support"]->resistance = 4;    $hordeoforcsUnits["magician"]->resistance = 5;
$hordeoforcsUnits["siege"]->speed      = 4;      $hordeoforcsUnits["support"]->speed      = 2;    $hordeoforcsUnits["magician"]->speed      = 3;
$hordeoforcsUnits["siege"]->range      = 1;      $hordeoforcsUnits["support"]->range      = 4;    $hordeoforcsUnits["magician"]->range      = 5;
$hordeoforcsUnits["siege"]->price      = 200;      $hordeoforcsUnits["support"]->price      = 200;    $hordeoforcsUnits["magician"]->price      = 225;
$hordeoforcsUnits["siege"]->population = 3;      $hordeoforcsUnits["support"]->population = 3;    $hordeoforcsUnits["magician"]->population = 3;
$hordeoforcsUnits["siege"]->entityType = 3;      $hordeoforcsUnits["support"]->entityType = 4;    $hordeoforcsUnits["magician"]->entityType = 5;

                        $hordeoforcsUnits["protector"]->health     = 195;      $hordeoforcsUnits["conqueror"]->health     = 290;
                        $hordeoforcsUnits["protector"]->damage     = 37;      $hordeoforcsUnits["conqueror"]->damage     = 46;
                        $hordeoforcsUnits["protector"]->defense    = 6;      $hordeoforcsUnits["conqueror"]->defense    = 7;
                        $hordeoforcsUnits["protector"]->resistance = 6;      $hordeoforcsUnits["conqueror"]->resistance = 3;
                        $hordeoforcsUnits["protector"]->speed      = 5;      $hordeoforcsUnits["conqueror"]->speed      = 4;
                        $hordeoforcsUnits["protector"]->range      = 1;      $hordeoforcsUnits["conqueror"]->range      = 1;
                        $hordeoforcsUnits["protector"]->price      = 300;      $hordeoforcsUnits["conqueror"]->price      = 400;
                        $hordeoforcsUnits["protector"]->population = 4;      $hordeoforcsUnits["conqueror"]->population = 2;
                        $hordeoforcsUnits["protector"]->entityType = 6;      $hordeoforcsUnits["conqueror"]->entityType = 7;

// Undead Legion
$undeadLegionUnits["leader"]->health     = 180;   $undeadLegionUnits["soldier"]->health     = 120;   $undeadLegionUnits["shooter"]->health     = 72;
$undeadLegionUnits["leader"]->damage     = 32;    $undeadLegionUnits["soldier"]->damage     = 28;   $undeadLegionUnits["shooter"]->damage     = 29;
$undeadLegionUnits["leader"]->defense    = 3;     $undeadLegionUnits["soldier"]->defense    = 3;    $undeadLegionUnits["shooter"]->defense    = 2;
$undeadLegionUnits["leader"]->resistance = 6;     $undeadLegionUnits["soldier"]->resistance = 4;    $undeadLegionUnits["shooter"]->resistance = 5;
$undeadLegionUnits["leader"]->speed      = 3;     $undeadLegionUnits["soldier"]->speed      = 3;    $undeadLegionUnits["shooter"]->speed      = 4;
$undeadLegionUnits["leader"]->range      = 5;     $undeadLegionUnits["soldier"]->range      = 1;    $undeadLegionUnits["shooter"]->range      = 5;
$undeadLegionUnits["leader"]->price      = 0;     $undeadLegionUnits["soldier"]->price      = 100;  $undeadLegionUnits["shooter"]->price      = 125;
$undeadLegionUnits["leader"]->population = 0;     $undeadLegionUnits["soldier"]->population = 2;    $undeadLegionUnits["shooter"]->population = 2;
$undeadLegionUnits["leader"]->entityType = 0;     $undeadLegionUnits["soldier"]->entityType = 1;    $undeadLegionUnits["shooter"]->entityType = 2;

$undeadLegionUnits["siege"]->health     = 195;      $undeadLegionUnits["support"]->health     = 78;    $undeadLegionUnits["magician"]->health     = 84;
$undeadLegionUnits["siege"]->damage     = 110;      $undeadLegionUnits["support"]->damage     = 28;    $undeadLegionUnits["magician"]->damage     = 32;
$undeadLegionUnits["siege"]->defense    = 3;      $undeadLegionUnits["support"]->defense    = 2;    $undeadLegionUnits["magician"]->defense    = 3;
$undeadLegionUnits["siege"]->resistance = 5;      $undeadLegionUnits["support"]->resistance = 5;    $undeadLegionUnits["magician"]->resistance = 8;
$undeadLegionUnits["siege"]->speed      = 5;      $undeadLegionUnits["support"]->speed      = 4;    $undeadLegionUnits["magician"]->speed      = 4;
$undeadLegionUnits["siege"]->range      = 4;      $undeadLegionUnits["support"]->range      = 6;    $undeadLegionUnits["magician"]->range      = 4;
$undeadLegionUnits["siege"]->price      = 150;      $undeadLegionUnits["support"]->price      = 200;    $undeadLegionUnits["magician"]->price      = 225;
$undeadLegionUnits["siege"]->population = 3;      $undeadLegionUnits["support"]->population = 3;    $undeadLegionUnits["magician"]->population = 3;
$undeadLegionUnits["siege"]->entityType = 3;      $undeadLegionUnits["support"]->entityType = 4;    $undeadLegionUnits["magician"]->entityType = 5;

                        $undeadLegionUnits["protector"]->health     = 168;      $undeadLegionUnits["conqueror"]->health     = 250;
                        $undeadLegionUnits["protector"]->damage     = 35;      $undeadLegionUnits["conqueror"]->damage     = 44;
                        $undeadLegionUnits["protector"]->defense    = 3;      $undeadLegionUnits["conqueror"]->defense    = 4;
                        $undeadLegionUnits["protector"]->resistance = 6;      $undeadLegionUnits["conqueror"]->resistance = 10;
                        $undeadLegionUnits["protector"]->speed      = 3;      $undeadLegionUnits["conqueror"]->speed      = 6;
                        $undeadLegionUnits["protector"]->range      = 1;      $undeadLegionUnits["conqueror"]->range      = 1;
                        $undeadLegionUnits["protector"]->price      = 300;      $undeadLegionUnits["conqueror"]->price      = 400;
                        $undeadLegionUnits["protector"]->population = 4;      $undeadLegionUnits["conqueror"]->population = 2;
                        $undeadLegionUnits["protector"]->entityType = 6;      $undeadLegionUnits["conqueror"]->entityType = 7;

// Barbarian Tribes
$barbarianTribesUnits["leader"]->health     = 175;   $barbarianTribesUnits["soldier"]->health     = 98;   $barbarianTribesUnits["shooter"]->health     = 90;
$barbarianTribesUnits["leader"]->damage     = 29;    $barbarianTribesUnits["soldier"]->damage     = 32;   $barbarianTribesUnits["shooter"]->damage     = 30;
$barbarianTribesUnits["leader"]->defense    = 3;     $barbarianTribesUnits["soldier"]->defense    = 3;    $barbarianTribesUnits["shooter"]->defense    = 3;
$barbarianTribesUnits["leader"]->resistance = 7;     $barbarianTribesUnits["soldier"]->resistance = 2;    $barbarianTribesUnits["shooter"]->resistance = 2;
$barbarianTribesUnits["leader"]->speed      = 4;     $barbarianTribesUnits["soldier"]->speed      = 4;    $barbarianTribesUnits["shooter"]->speed      = 5;
$barbarianTribesUnits["leader"]->range      = 4;     $barbarianTribesUnits["soldier"]->range      = 1;    $barbarianTribesUnits["shooter"]->range      = 6;
$barbarianTribesUnits["leader"]->price      = 0;     $barbarianTribesUnits["soldier"]->price      = 100;  $barbarianTribesUnits["shooter"]->price      = 125;
$barbarianTribesUnits["leader"]->population = 0;     $barbarianTribesUnits["soldier"]->population = 2;    $barbarianTribesUnits["shooter"]->population = 2;
$barbarianTribesUnits["leader"]->entityType = 0;     $barbarianTribesUnits["soldier"]->entityType = 1;    $barbarianTribesUnits["shooter"]->entityType = 2;

$barbarianTribesUnits["siege"]->health     = 170;      $barbarianTribesUnits["support"]->health     = 80;    $barbarianTribesUnits["magician"]->health     = 86;
$barbarianTribesUnits["siege"]->damage     = 120;      $barbarianTribesUnits["support"]->damage     = 27;    $barbarianTribesUnits["magician"]->damage     = 29;
$barbarianTribesUnits["siege"]->defense    = 2;      $barbarianTribesUnits["support"]->defense    = 2;    $barbarianTribesUnits["magician"]->defense    = 3;
$barbarianTribesUnits["siege"]->resistance = 2;      $barbarianTribesUnits["support"]->resistance = 5;    $barbarianTribesUnits["magician"]->resistance = 5;
$barbarianTribesUnits["siege"]->speed      = 2;      $barbarianTribesUnits["support"]->speed      = 2;    $barbarianTribesUnits["magician"]->speed      = 3;
$barbarianTribesUnits["siege"]->range      = 11;      $barbarianTribesUnits["support"]->range      = 5;    $barbarianTribesUnits["magician"]->range      = 6;
$barbarianTribesUnits["siege"]->price      = 150;      $barbarianTribesUnits["support"]->price      = 200;    $barbarianTribesUnits["magician"]->price      = 225;
$barbarianTribesUnits["siege"]->population = 3;      $barbarianTribesUnits["support"]->population = 3;    $barbarianTribesUnits["magician"]->population = 3;
$barbarianTribesUnits["siege"]->entityType = 3;      $barbarianTribesUnits["support"]->entityType = 4;    $barbarianTribesUnits["magician"]->entityType = 5;

                        $barbarianTribesUnits["protector"]->health     = 225;      $barbarianTribesUnits["conqueror"]->health     = 315;
                        $barbarianTribesUnits["protector"]->damage     = 38;      $barbarianTribesUnits["conqueror"]->damage     = 47;
                        $barbarianTribesUnits["protector"]->defense    = 5;      $barbarianTribesUnits["conqueror"]->defense    = 4;
                        $barbarianTribesUnits["protector"]->resistance = 4;      $barbarianTribesUnits["conqueror"]->resistance = 5;
                        $barbarianTribesUnits["protector"]->speed      = 4;      $barbarianTribesUnits["conqueror"]->speed      = 4;
                        $barbarianTribesUnits["protector"]->range      = 1;      $barbarianTribesUnits["conqueror"]->range      = 1;
                        $barbarianTribesUnits["protector"]->price      = 300;      $barbarianTribesUnits["conqueror"]->price      = 400;
                        $barbarianTribesUnits["protector"]->population = 4;      $barbarianTribesUnits["conqueror"]->population = 2;
                        $barbarianTribesUnits["protector"]->entityType = 6;      $barbarianTribesUnits["conqueror"]->entityType = 7;


// Faction Intializer
$factions["empire"]         = new \Factions\Faction("empire", "Empire", "Císařství", $empireUnits);
$factions["elvenKingdom"]   = new \Factions\Faction("elven_kingdom", "Elven Kingdom","", $elvenKingdomUnits);
$factions["mountDwarves"]   = new \Factions\Faction("tribe_of_mountain_dwarves", "Tribe of Mountain Dwarves","", $mountainDwarvesUnits);
$factions["hordeOfOrcs"]    = new \Factions\Faction("horde_of_orcs", "Horde of Orcs","", $hordeoforcsUnits);
$factions["undeadLegion"]   = new \Factions\Faction("undead_legion", "Undead Legion","", $undeadLegionUnits);
$factions["barbarianTribes"]= new \Factions\Faction("barbarian_tribes", "Barbarian Tribes","", $barbarianTribesUnits);


// Unset all variables except $factions

unset($empireUnits);
unset($elvenKingdomUnits);
unset($barbarianTribesUnits);
unset($hordeoforcsUnits);
unset($undeadLegionUnits);
unset($mountainDwarvesUnits);