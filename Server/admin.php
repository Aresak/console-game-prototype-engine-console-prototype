<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 14.05.2016
 * Time: 17:53
 */

// includes
session_start();
define("allowed_to_view_database_info", true);
include_once "../../../database.php";
include_once "../../../Cryospark.php";

function notLogged($r = "") {
    header("Location: /BattleForTreasure/special.php?gamemasterrequired$r");
    die("No rights to see this");
}

if(isset($_SESSION["sessionkey"])) {
    $sessionkey = $_SESSION["sessionkey"];
} else notLogged("?notlogged");

$con = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
    or die(mysqli_error($con));

$account = new \BFT\Account();
if(!$account->loadObject($con, "sessionkey", $sessionkey)) {
    notLogged("?notlogged");
}

$gamekey = new \BFT\GameKey();
if(!$gamekey->loadObject($con, "owner_id", $account->ID)) {
    notLogged("?notverified");
}

if($gamekey->speciality != 2) {
    notLogged("?spec=" . $gamekey->speciality);
}

// Welcome,
// master

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>HTTPServer | Battle for Treasure</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="http://account.symbiant.cz/media/favicon.ico" rel="icon" type="image/x-icon" />

        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/mainstyle.css">
        <link rel="stylesheet" href="/BattleForTreasure/bftstyle.css">

        <!-- Scripts -->
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </head>

    <body>

        <div class="container">
            <div class="page-header">
                <h1>Battle for Treasure HTTPServer <small>Administration</small></h1>
            </div>

            <div class="panel panel-default">
                <!-- Server list -->

                <form class="form form-default" action="/BattleForTreasure/HTTPServer/Server/window.php" method="POST">
                    Password <input type="text" name="password" placeholder="Password"> | <input type="number" min="4" max="60" value="10" name="timeout" placeholder="Timeout"> Timeout<br>
                    Refresh Rate <input type="number" name="refresh" min="100" max="50000" value="1000"> | <button>START NEW SERVER</button>
                </form>

                <br><br><br>

                <hr>

                <table id="serverlist" class="table table-striped">
                </table>
            </div>
        </div>

    </body>
</html>

<script>
    function refreshServerList() {
        $("#serverlist").load("serverlist.php");
        setTimeout(refreshServerList, 1000);
    }

    refreshServerList();
</script>
