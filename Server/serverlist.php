<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 14.05.2016
 * Time: 19:53
 */
?>

    <tr class=" text-center">
        <td>
            <strong>#</strong>
        </td>

        <td>
            IP
        </td>

        <td>
            Server status
        </td>

        <td>
            Clients
        </td>

        <td>
            Stage
        </td>

        <td>
            Locked
        </td>

        <td>
            Password
        </td>

        <td>
            Server ID
        </td>

        <td>
            Game ID
        </td>

        <td>
            Last Ping
        </td>

        <td>
            Timeout
        </td>
    </tr>

<?php
define("allowed_to_view_database_info", true);
include "../../../database.php";


// prepare the connection
$con = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
or die(mysqli_error($con));

// Query list
$serversListQuery = "SELECT * FROM bft_ws1_servers";
$serversListResult = mysqli_query($con, $serversListQuery)
    or die(mysqli_error($con));

$serverIDs = array();

// check every server now
$g = false;
for($i = 0; $i < mysqli_num_rows($serversListResult); $i ++) {
    $g = true;
    // check the ping
    // if too long
    // remove the server
    if((time() - mysqli_result($serversListResult, $i, "last_ping")) > mysqli_result($serversListResult, $i, "timeout")) {
        // the server is offline probably, or crashed
        $deleteServerQuery = "DELETE FROM bft_ws1_servers WHERE ID='" . mysqli_result($serversListResult, $i, 'ID') . "'";
        $deleteServerResult = mysqli_query($con, $deleteServerQuery)
        or die(mysqli_error($con));
        // and don't show on the list, or disabled!
    }
    else {
        // All is good, show this item!
        // todo stage
        $stage      = "Waiting";
        $game_id     = mysqli_result($serversListResult, $i, "game_id");
        $players    = mysqli_result($serversListResult, $i, "players");
        $id         = mysqli_result($serversListResult, $i, "ID");
        $locked     = mysqli_result($serversListResult, $i, "locked");
        $password   = mysqli_result($serversListResult, $i, "password");
        $server_id  = mysqli_result($serversListResult, $i, "server_id");
        $lastping   = mysqli_result($serversListResult, $i, "last_ping");
        $lastping   = time() - $lastping;
        $timeout    = mysqli_result($serversListResult, $i, "timeout");
        $ip         = mysqli_result($serversListResult, $i, "IP");

        // add to the servers list
        $serverIDs[$server_id] = $game_id;

        echo "<tr class='serverItem text-center' onClick=\"spectate('$id')\">";
        echo "<td>$id</td>";
        echo "<td>$ip</td>";
        echo "<td class='online'>ONLINE</td>";
        echo "<td>$players</td>";
        echo "<td>$stage</td>";
        echo "<td>$locked</td>";
        echo "<td>$password</td>";
        echo "<td>$server_id</td>";
        echo "<td>$game_id</td>";
        echo "<td>$lastping</td>";
        echo "<td>$timeout</td>";
        echo "</tr>";
    }
}

// also check the games
$gamesListQuery = "SELECT * FROM bft_ws1_game";
$gamesListResult = mysqli_query($con, $gamesListQuery)
    or die(mysqli_error($con));

for($a = 0; $a < mysqli_num_rows($gamesListResult); $a ++) {
    if(!isset($serverIDs[ mysqli_result($gamesListResult, $a, "server_id") ])) {
        // oh there is no server
        // probably crashed
        // or timed out
        // or anything wut

        // delete the game
        // todo finish this mate
    }
}

if(!$g) echo "<tr class='alert alert-danger text-center'><td></td><td>No servers online!</td><td>No servers online!</td><td>No servers online!</td><td>No servers online!</td><td>No servers online!</td><td>No servers online!</td><td>No servers online!</td><td>No servers online!</td><td>No servers online!</td><td>No servers online!</td></tr>";