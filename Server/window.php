<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 14.05.2016
 * Time: 19:32
 */

// This is the server. If there is opened window,
// the server is online

// includes
session_start();
define("allowed_to_view_database_info", true);
include_once "../../../database.php";
include_once "../../../Cryospark.php";

function notLogged($r = "")
{
    header("Location: /BattleForTreasure/special.php?gamemasterrequired$r");
    die("No rights to see this");
}

if (isset($_SESSION["sessionkey"])) {
    $sessionkey = $_SESSION["sessionkey"];
} else notLogged("?notlogged");

$con = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
or die(mysqli_error($con));

$account = new \BFT\Account();
if (!$account->loadObject($con, "sessionkey", $sessionkey)) {
    notLogged("?notlogged");
}

$gamekey = new \BFT\GameKey();
if (!$gamekey->loadObject($con, "owner_id", $account->ID)) {
    notLogged("?notverified");
}

if ($gamekey->speciality != 2) {
    notLogged("?spec=" . $gamekey->speciality);
}

// Welcome,
// master

// Error list
// 43: No post up
// 44: No password *
// 45: No timeout  *

if (!isset($_POST)) die("Error #43");
// Else it's setup


// / Security weird stuff could be here to have it safer


// / Security ende
// Game, Management

if (isset($_POST["password"])) {
    if (isset($_POST["timeout"])) {
        if (!isset($_POST["refresh"])) die("Error: 46");
        // Create new line
        $pw = $_POST["password"];
        $to = $_POST["timeout"];
        $re = $_POST["refresh"];
        $si = rand(100000, 999999);

        $scq = "SELECT * FROM bft_ws1_configs";
        $scr = mysqli_query($con, $scq) or die(mysqli_error($con));
        $sc = mysqli_result($scr, 0, "data");

        $newServerQuery = "INSERT INTO bft_ws1_servers (ip, refreshRate, server_id, password, timeout, last_ping, players, serverConfig) VALUES ('" . $_SERVER["REMOTE_ADDR"] . "', '$re', '$si', '$pw', '$to', '" . time() . "', '0', '$sc')";
        $newServerResult = mysqli_query($con, $newServerQuery)
        or die(mysqli_error($con));

        // Server is now on
        // get the data and put it into javascript

        ?>

        <html lang="en">
        <head>
            <title>BfT HTTPServer | <?php echo $si; ?></title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <link href="http://account.symbiant.cz/media/favicon.ico" rel="icon" type="image/x-icon"/>

            <!-- CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
            <link rel="stylesheet" href="/mainstyle.css">
            <link rel="stylesheet" href="/BattleForTreasure/bftstyle.css">

            <!-- Scripts -->
            <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        </head>

        <body>

        <div id="manualWork">
            <button class="button btn-default" onClick="manualRefresh();">Manual Refresh</button>
        </div>

        <br>
        <br>
        <br>
        <br>
        <br>

        <div id="players">
            <table style="border: 1px solid black; width: 100%; position: relative; left: 0; top: 0;">
                <tr>
                    <td>

                    </td>
                    <td>
                        <b>
                            ID
                        </b>
                    </td>
                    <td>
                        <b>
                            Username
                        </b>
                    </td>
                    <td>
                        <b>
                            Display Name
                        </b>
                    </td>
                    <td>
                        <b>
                            Email
                        </b>
                    </td>
                    <td>
                        <b>
                            Game Key
                        </b>
                    </td>
                    <td>
                        <b>
                            Friends
                        </b>
                    </td>
                </tr>
                <?php

                // too lazy for this

                for($i = 1; $i <= 2; $i ++) {
                    echo "<tr>";
                    echo "<td>Player $i</td>";
                    echo "<td id='pid$i'></td>";
                    echo "<td id='pus$i'></td>'";
                    echo "<td id='pdn$i'></td>";
                    echo "<td id='pem$i'></td>";
                    echo "<td id='pgk$i'></td>";
                    echo "<td id='pfr$i'></td>";
                    echo "</tr>";
                }
                ?>
            </table>
        </div>
        <hr>
        <br>

        <div id="game">
            <table style="border: 1px solid black; width: 100%; position: relative; left: 0; top: 0;">
                <tr>
                    <td align="right">
                        <b>
                            ID
                        </b>
                    </td>
                    <td id="gid"></td>

                    <td align="right">
                        <b>
                            Round
                        </b>
                    </td>
                    <td id="sid"></td>
                </tr>

                <tr>
                    <td align="right">
                        <b>
                            Turn
                        </b>
                    </td>
                    <td id="sid"></td>

                    <td align="right">
                        <b>
                            Turn Total
                        </b>
                    </td>
                    <td id="sid"></td>
                </tr>

                <tr>
                    <td align="right">
                        <b>
                            Timer
                        </b>
                    </td>
                    <td id="sid"></td>

                    <td align="right">
                        <b>
                            Stage
                        </b>
                    </td>
                    <td id="sid"></td>
                </tr>

                <tr>
                    <td align="right">
                        <b>
                            Sync Time
                        </b>
                    </td>
                    <td id="sid"></td>
                    <td></td><td></td>
                </tr>
            </table>
        </div>
        <br>

        <div id="server" style="border: 1px solid black; width: 100%; position: relative; left: 0; top: 0;">
            <table>
                <tr>
                    <td align="right">
                        <b>
                            ID
                        </b>
                    </td>
                    <td id="sid"></td>

                    <td align="right">
                        <b>
                            Server ID
                        </b>
                    </td>
                    <td id="ssid"></td>
                </tr>

                <tr>
                    <td align="right">
                        <b>
                            Game ID
                        </b>
                    </td>
                    <td id="sgid"></td>

                    <td align="right">
                        <b>
                            Players
                        </b>
                    </td>
                    <td id="spl"></td>
                </tr>

                <tr>
                    <td align="right">
                        <b>
                            Locked
                        </b>
                    </td>
                    <td id="slo"></td>

                    <td align="right">
                        <b>
                            Password
                        </b>
                    </td>
                    <td id="spass"></td>
                </tr>

                <tr>
                    <td align="right">
                        <b>
                            Last ping
                        </b>
                    </td>
                    <td id="slp"></td>

                    <td align="right">
                        <b>
                            Timeout
                        </b>
                    </td>
                    <td id="sto"></td>
                </tr>

                <tr>
                    <td align="right">
                        <b>
                            Refresh Rate
                        </b>
                    </td>
                    <td id="srr"></td>

                    <td align="right">
                        <b>
                            IP
                        </b>
                    </td>
                    <td id="sip"></td>
                </tr>

                <tr>
                    <td align="right">
                        <b>
                            Config
                        </b>
                    </td>
                    <td id="scfg"></td>
                    <td></td><td></td>
                </tr>
            </table>
        </div>
        <br>

        <div id="serverLog"></div>
        <br>
        <pre id="jsonResult"></pre>
        </body>
        </html>
        <script>
            var serverid = <?php echo $si; ?>;
            var refreshRate = <?php echo $re; ?>;
            function manualRefresh() {
                $.post("ajax.php", {action: "refresh", server: serverid}, function (data) {
                    workWithData(data);
                }, "html");
            }

            function refreshData() {
                // data returned in json
                $.post("ajax.php", {action: "refresh", server: serverid}, function (data) {
                    workWithData(data);
                }, "json");
                setTimeout(refreshData, refreshRate);
            }

            function workWithData(data) {
                console.log("Update: " + JSON.stringify(data));
                $("#pid1").text(data.player1.ID);
                $("#pus1").text(data.player1.username);
                $("#pdn1").text(data.player1.display_name);
                $("#pem1").text(data.player1.email);
                $("#pgk1").text(data.player1.game_key);
                $("#pfr1").text(data.player1.friendlist.friends);

                $("#pid2").text(data.player2.ID);
                $("#pus2").text(data.player2.username);
                $("#pdn2").text(data.player2.display_name);
                $("#pem2").text(data.player2.email);
                $("#pgk2").text(data.player2.game_key);
                $("#pfr2").text(data.player2.friendlist.friends);

                output(syntaxHighlight(JSON.stringify(data, undefined, 4)));
            }

            refreshData();

            function output(inp) {
                $("#jsonResult").html(inp);
            }

            function syntaxHighlight(json) {
                json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                    var cls = 'number';
                    if (/^"/.test(match)) {
                        if (/:$/.test(match)) {
                            cls = 'key';
                        } else {
                            cls = 'string';
                        }
                    } else if (/true|false/.test(match)) {
                        cls = 'boolean';
                    } else if (/null/.test(match)) {
                        cls = 'null';
                    }
                    return '<span class="' + cls + '">' + match + '</span>';
                });
            }
        </script>
        <style>
            #jsonResult {
                outline: 1px solid #ccc;
                padding: 5px;
                margin: 5px;
            }

            .string {
                color: green;
            }

            .number {
                color: darkorange;
            }

            .boolean {
                color: blue;
            }

            .null {
                color: magenta;
            }

            .key {
                color: red;
            }
        </style>
        <?php
    } else die("Error #45");
} else die("Error #44");