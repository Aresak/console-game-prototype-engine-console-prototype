<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 14.05.2016
 * Time: 20:39
 */


ini_set('display_errors', 'On');
error_reporting(E_ALL);

$jsonResult = array();
$jsonResult["processStarted"] = microtime();
$jsonResult["Result"] = "";
$jsonResult["debug"] = null;


if (isset($_POST["action"])) {
    $action = $_POST["action"];

    if ($action == "refresh") {
        // That's whatsup
        // do all the logic now here
        define("allowed_to_view_database_info", true);
        include "../../../database.php";
        include "../../../Cryospark.php";


        // prepare the connection
        $con = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
        or die(mysqli_error($con));

        $server = $_POST["server"]; // server id
        // load server config
        $serverData = new \BFT\Server();
        if (!$serverData->loadObject($con, "server_id", $server)) {
            // we timed out, or just the server crashed
            // ... ... ... the server doesnt exists in matrix
            $jsonResult["Result"] = "serverTimedOut";
            dieWithJSON($jsonResult);
        }

        $timeout = $serverData->timeout;

        $deltime = time() - 30;
        $gamesQuery = "DELETE FROM bft_ws1_game WHERE synctime<'$deltime'";
        $gamesResult = mysqli_query($con, $gamesQuery)
        or die(mysqli_error($con));


        // get JSON server info
        $jsonResult["server"] = $serverData->array;
        $jsonResult["server"]["configJSON"] = json_decode(file_get_contents($serverData->serverConfig));
        $jsonResult["processes"] = array();

        // update ping
        $serverData->updateObject($con, "last_ping", time());

        // first check if is there a game on
        if ($serverData->game_id == 0) {
            // there is no game on
            // create a new game
            $gi = rand(100000, 999999);
            $newGameQuery = "INSERT INTO bft_ws1_game (server_id, synctime, game_id) VALUES ('$server', '" . time() . "', '$gi')";
            $newGameResult = mysqli_query($con, $newGameQuery)
                or die(mysqli_error($con));

            // get game id
            $game = new \BFT\Game();
            $game->loadObject($con, "server_id", $serverData->server_id);


                // update server info
            $updateQuery = "UPDATE bft_ws1_servers SET last_ping='" . time() . "', game_id='$gi' WHERE server_id='$server'";
            $updateResult = mysqli_query($con, $updateQuery)
                or die(mysqli_error($con));


            // load data into json for administration and other works
            // ...

            $game->updateObject($con, "synctime", time());
            $jsonResult["game"] = $game->array;
        } else {
            // We will load our game
            $game = new \BFT\Game();
            $game->loadObject($con, "server_id", $serverData->server_id);
            $game->updateObject($con, "synctime", time());
            $jsonResult["game"] = $game->array;
        }

        // there are not enough players online, so wait for them
        $rev = $serverData->players;
        if ($rev != 2) {
            $jsonResult["debug"]["waiting"] = true;
            $rQuery = "SELECT * FROM bft_ws1_queue WHERE game='" . $game->game_id . "'";
            $rResult = mysqli_query($con, $rQuery)
                or die(mysqli_error($con));
            $rev += mysqli_num_rows($rResult);
            $jsonResult["debug"]["selected_to_us"] = $rev;
            $playerQueueListQuery = "SELECT * FROM bft_ws1_queue WHERE game=0 AND ping>'" . (time() - $jsonResult["server"]["timeout"]) . "'";
            $playerQueueListResult = mysqli_query($con, $playerQueueListQuery)
            or die(mysqli_error($con));

            $jsonResult["debug"]["waitingPlayers"] = mysqli_num_rows($playerQueueListResult);
            if (mysqli_num_rows($playerQueueListResult) > 0 && $rev != 2) {
                // we got someone in the queue!
                $queue = rand(0, mysqli_num_rows($playerQueueListResult) - 1);
                // this is our lucky guy
                // get the lucky guy info
                $jsonResult["queue"]["requested"]["ID"] = mysqli_result($playerQueueListResult, $queue, "ID");
                $jsonResult["queue"]["requested"]["client_id"] = mysqli_result($playerQueueListResult, $queue, "client_id");
                $jsonResult["queue"]["requested"]["jointime"] = mysqli_result($playerQueueListResult, $queue, "jointime");
                $jsonResult["queue"]["requested"]["ping"] = mysqli_result($playerQueueListResult, $queue, "ping");
                $jsonResult["queue"]["requested"]["game"] = mysqli_result($playerQueueListResult, $queue, "game");
                $jsonResult["queue"]["requested"]["game"]["n"] = $jsonResult["game"]["ID"];
                $jsonResult["processes"]["queueRequested"] = true;

                $queue = new \BFT\Queue();
                $queue->loadObject($con, "ID", $jsonResult["queue"]["requested"]["ID"]);
                $queue->updateObject($con, "game", $jsonResult["game"]["game_id"]);
            }

            //$jsonResult["debug"]["queue"] = var_dump($queue);

            // that's all we want to do now

        }
        // we got all players in whatsup now then

        // this will work
        include "BattleForTreasure.php";

        // preload player info
        $jsonResult["player1"]["ID"] = 0;
        $jsonResult["player1"]["username"] = "";
        $jsonResult["player1"]["display_name"] = "";
        $jsonResult["player1"]["password"] = "";
        $jsonResult["player1"]["email"] = "";
        $jsonResult["player1"]["register_date"] = 0;
        $jsonResult["player1"]["public_key"] = "";
        $jsonResult["player1"]["private_key"] = "";
        $jsonResult["player1"]["password_salt"] = "";
        $jsonResult["player1"]["game_key"] = "";
        $jsonResult["player1"]["activated"] = false;
        $jsonResult["player1"]["activationKey"] = "";
        $jsonResult["player1"]["sessionkey"] = "";
        $jsonResult["player1"]["friendlist"] = BattleForTreasure\Friendlist::emptyFriendlist();;

        $jsonResult["player2"]["ID"] = 0;
        $jsonResult["player2"]["username"] = "";
        $jsonResult["player2"]["display_name"] = "";
        $jsonResult["player2"]["password"] = "";
        $jsonResult["player2"]["email"] = "";
        $jsonResult["player2"]["register_date"] = 0;
        $jsonResult["player2"]["public_key"] = "";
        $jsonResult["player2"]["private_key"] = "";
        $jsonResult["player2"]["password_salt"] = "";
        $jsonResult["player2"]["game_key"] = "";
        $jsonResult["player2"]["activated"] = false;
        $jsonResult["player2"]["activationKey"] = "";
        $jsonResult["player2"]["sessionkey"] = "";
        $jsonResult["player2"]["friendlist"] = BattleForTreasure\Friendlist::emptyFriendlist();;

        // data load
        if ($game->player1_id != 0) {
            // set Player 1 info
            $playerOne  = new \BFT\Account();
            $playerOne->loadObject($con, "ID", $game->player1_id);

            $jsonResult["player1"] = $playerOne->array;
            if (empty($playerOne->friendlist))
                $jsonResult["player1"]["friendlist"] = BattleForTreasure\Friendlist::emptyFriendlist();
        }

        if ($jsonResult["game"]["player2_id"] != 0) {
            // set Player 2 info
            $playerTwo  = new \BFT\Account();
            $playerTwo->loadObject($con, "ID", $game->player2_id);

            $jsonResult["player2"] = $playerTwo->array;
            if (empty($playerTwo->friendlist))
                $jsonResult["player2"]["friendlist"] = BattleForTreasure\Friendlist::emptyFriendlist();
        }

        $jsonResult["BattleForTreasure"] = BattleForTreasure\BattleForTreasure::emptyResultArray();
        if ($serverData->players == 2) {
            // Stages up
            if ($game->stage == 0) {
                $game->updateObject($con, "stage", 1);

                // Who's choosing an faction
                $lucky = rand(1, 2);
                $game->updateObject($con, "turn", $jsonResult["player" . $lucky]["ID"]);
            }


            // Get Game Moves Data
            $gameMovesDataQuery = "SELECT * FROM bft_ws1_gamemoves WHERE game_id='" . $jsonResult["game"]["ID"] . "'";
            $gameMovesDataResult = mysqli_query($con, $gameMovesDataQuery)
            or die(mysqli_error($con));

            $gameMoves = array();
            for ($gameMove = 0; $gameMove < mysqli_num_rows($gameMovesDataResult); $gameMove++) {
                $gameMoves[$gameMove]["ID"] = mysqli_result($gameMovesDataResult, $gameMove, "ID");
                $gameMoves[$gameMove]["game_id"] = mysqli_result($gameMovesDataResult, $gameMove, "game_id");
                $gameMoves[$gameMove]["type"] = mysqli_result($gameMovesDataResult, $gameMove, "type");
                $gameMoves[$gameMove]["data1"] = mysqli_result($gameMovesDataResult, $gameMove, "data1");
                $gameMoves[$gameMove]["data2"] = mysqli_result($gameMovesDataResult, $gameMove, "data2");
                $gameMoves[$gameMove]["data3"] = mysqli_result($gameMovesDataResult, $gameMove, "data3");
                $gameMoves[$gameMove]["data4"] = mysqli_result($gameMovesDataResult, $gameMove, "data4");
                $gameMoves[$gameMove]["data5"] = mysqli_result($gameMovesDataResult, $gameMove, "data5");
                $gameMoves[$gameMove]["extra_data"] = mysqli_result($gameMovesDataResult, $gameMove, "extra_data");
            }

            // Battle for Treasure
            // Instantiate the BfT object
            $bft = new BattleForTreasure\BattleForTreasure($jsonResult, $gameMoves);

            // Process Automatte turns
            $bft->processMoves();

            // Get The JSON Result
            $jsonResult["BfT"] = $bft->getResult();
        } else {
            if ($game->stage != 0) {
                // User disconnected?
                $jsonResult["processes"]["user_disconnected"] = true;
                $jsonResult["processes"]["users_now"] = $serverData->players;
            }
        }


    } else dieWithJSON($jsonResult, "Error 1");
} else dieWithJSON($jsonResult, "Error 2");


dieWithJSON($jsonResult);

function dieWithJSON($json, $extras = "")
{
    $json["extras"] = $extras;
    $json["processEnded"] = microtime();
    die(json_encode($json));
}