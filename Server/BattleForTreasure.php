<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 26.05.2016
 * Time: 14:54
 */

namespace BattleForTreasure;

class Friendlist {
    public static function emptyFriendlist() {
        $fl = array();

        $fl["friends"] = 0;
        $fl["list"] = array();
        $fl["list"][0]["display_name"] = "";
        $fl["list"][0]["public_key"]   = "";
        $fl["list"][0]["lastseen"]     = 0;
        $fl["list"][0]["online"]       = false;

        return $fl;
    }
}


class BattleForTreasure {
    // just some really amateur weird variable here
    public $playerOnLeft    = 1;        // If not correct just change uh



    public $DataArray       = array();  // Collects data about server, game and players
    public $GameMoves       = array();  // Collects logical game data

    public $ResultAway      = array();  // What to return back to the server

    private $updatedMoves   = array();  // What moves are new and processed
    private $processArray   = array();  // Sorted Array to be done :: Processed GameMoves array

    private $players        = array();  // Players game data


    private $mapData        = array();  // Contains map data
    private $mapDataExtras  = array();  // May contain some extras data for map
    //      mapData Array Structure
    //      =======================
    //      mapData (void)
    //          TileX   (int)
    //              TileY   (int)
    //                  Tile Type       [TileType (int)]
    //                  Occupied        (boolean)
    //                  Entity          [EntityType (int)]

    private $unitData       = array();  // Contains units data
    //      unitData Array Structure
    //      =========================
    //      unitData    (void)
    //          ID          (int)
    //              name        (String)
    //              price       (int)
    //              range       (int)
    //              health      (int)
    //              defense     (int)
    //              resistance  (int)
    //              speed       (int)
    //              bonus       (boolean)
    //              population  (int)
    //              player      (int)

    private $cardData       = array();  // Contains cards data

    private $monstersData   = array();  // Contains monsters data
    private $treasureData   = array();  // Contains treasure data

    private $switchMoves = array(
        "screen"    => 1,
        "request"   => 2,
        "history"   => 3
    );

    // Functions
    // Rules:
    // - Reachable functions starts lowercase
    // - Private functions starts uppercase if not
    //                                              minor

    function __construct($DataArray, $GameMoves)
    {
        $this->DataArray = $DataArray;
        $this->GameMoves = $GameMoves;

        // Read through every move and sort it out
        $this->ReadThruMoves();
    }

    public function getResult() {
        return $this->ResultAway;
    }

    // Function that reads rules
    private function ReadThruMoves() {
        // Sort Game Moves
        for($moveIndex = 0; $moveIndex < sizeof($this->GameMoves); $moveIndex ++) {
            $this->processArray[strtr($this->GameMoves[$moveIndex]["type"], $this->switchMoves)] = $this->GameMoves[$moveIndex];
        }

        // And now process them

    }

    public function processMoves() {
        $processingArrayIndex = 0;
        foreach($this->processArray as $gameMove) {
            $gameMove["type"] = strtr($gameMove["type"], $this->switchMoves);

            switch($gameMove["type"]) {
                case 2:
                    // this is waiting to be processed
                    // Structure
                    // -- data1         :: Client Requested (Either Player1, Player2, Game, Unknown)
                    // -- data2         :: Request Type

                    // Request Dependency
                    //  data2 == BuildOnTile
                    //      -- data3    :: Tile To build on
                    //      -- data4    :: Building info
                    //  data2 == RecruitUnitOnTile
                    //      -- data3    :: Tile To recruit on
                    //      -- data4    :: Unit
                    //  data2 == MoveUnitOnTile
                    //      -- data3    :: Tile To move on
                    //      -- data4    :: Unit ID
                    //  data2 == LeaveGame
                    //      -- data3    :: Reason
                    //  data2 == SetActive Bonus
                    //      -- data3    :: Bonus Slot

                    if($gameMove["data2"] == "BuildOnTile") {
                        $player = ($gameMove["data1"] == "Player1" ? 1 : 2);

                        // let's check the money!
                        if($this->players[$player]["gold"] >= $gameMove["data4"]["price"]) {
                            // We have the money
                            if($this->canBuild($gameMove["data3"]["x"], $gameMove["data3"]["y"], $player)) {
                                // We can build
                                // change map tile
                                $this->BuildOnTile($gameMove["data3"]["x"], $gameMove["data3"]["y"], $gameMove["data4"]);
                            }
                            else {
                                $this->processArray[$processingArrayIndex]["type"] = 3;
                                $this->processArray[$processingArrayIndex]["extra_data"]["processed"] = "failed";
                                $this->processArray[$processingArrayIndex]["extra_data"]["processedFailedReason"] = "cant-build";
                            }
                        }
                        else {
                            $this->processArray[$processingArrayIndex]["type"] = 3;
                            $this->processArray[$processingArrayIndex]["extra_data"]["processed"] = "failed";
                            $this->processArray[$processingArrayIndex]["extra_data"]["processedFailedReason"] = "no-gold";
                        }
                    }
                    else if($gameMove["data2"] == "RecruitUnitOnTile") {
                        $player = ($gameMove["data1"] == "Player1" ? 1 : 2);

                        // let's check the money
                        if($this->players[$player]["gold"] >= $gameMove["data4"]["price"]) {
                            // We have the money
                            if($this->players[$player]["population"] < $gameMove["data4"]["population"]) {
                                // We got empty beds
                                if($this->canRecruit($gameMove["data3"]["x"], $gameMove["data3"]["y"], $player)) {
                                    // We can recruit
                                    $this->RecruitUnitOnTile($gameMove["data3"]["x"], $gameMove["data3"]["y"], $gameMove["data4"]);
                                }
                            }
                        }
                    }


                    break;

                case 3:
                    // this has been already processed and should be ignored
                    // not really for us, but if we want... Here you go


                    break;

                case 1:
                    // this is screen data
                    // Structure
                    // -- data1         ::  Player 1 Desk
                    // -- data2         ::  Player 2 Desk
                    // -- data3         ::  Map Desk
                    // -- data4         ::  Monsters Data
                    // -- data5         ::  Treasure Data
                    // -- extra_data    ::  Will include if is the latest screen data
                    $data1          = json_decode($gameMove["data1"]);
                    $data2          = json_decode($gameMove["data2"]);
                    $data3          = json_decode($gameMove["data3"]);
                    $data4          = json_decode($gameMove["data4"]);
                    $data5          = json_decode($gameMove["data5"]);
                    $extra_data     = json_decode($gameMove["extra_data"]);




                    $units  = $data1["units"] . $data2["units"];
                    $cards  = $data1["cards"] . $data2["cards"];

                    $this->unitData     = $units;
                    $this->cardData     = $cards;
                    $this->mapData      = $data3;
                    $this->monstersData = $data4;
                    $this->treasureData = $data5;
                    $this->mapDataExtras= $extra_data;
                    $this->players[1]   = $data1;
                    $this->players[2]   = $data2;

                    break;
            }

            $processingArrayIndex ++;
        }
    }

    // todo leave it atm
    private function MoveUnitOnTile() {

    }

    // todo sooon
    private function RecruitUnitOnTile($x, $y, $entity) {
        $this->mapData[$x][$y] = $entity;
        $this->mapData[$x][$y]["occupied"] = true;
    }

    // todo sooon
    private function BuildOnTile($x, $y, $entity) {
        $this->mapData[$x][$y] = $entity;
        $this->mapData[$x][$y]["occupied"] = true;
    }

    private function canBuild($x, $y, $type, $player = null) {
        // first check if there is any entity on the tile
        if($this->mapData[$x][$y]["entity"] == "Empty") {
            // if is not occupied
            if($this->mapData[$x][$y]["occupied"] == false) {
                // looks like it's an empty tile
                // but can we build?
                if($this->playerOnLeft == $player) {
                    // this is player on the left side of the map
                    if ($x != 3) { // So he is on 3rd tile
                        // this means he cannot build
                        return false;
                    }
                } else if($x != 17) {
                    // this is player on right side of the map
                    // and also not in the right column
                    return false;
                }

                // buildings: wall, tower, gate
                if($type == "gate") {
                    // this is gate
                    if($y == 11) {
                        // is this the center of the map
                        // we can build
                        return true;
                    }
                }

                else if($type == "tower") {
                    // this is tower
                    if($y == 4 || $y == 8 || $y == 14 || $y == 18) {
                        // we can build
                        return true;
                    }
                }

                else if($type == "wall") {
                    // we can build
                    return true;
                }
            }
        }

        return false;
    }

    private function canRecruit($x, $y, $player = null) {
        // first check if there is any entity on the tile
        if($this->mapData[$x][$y]["entity"] == "Empty") {
            // if is not occupied
            if ($this->mapData[$x][$y]["occupied"] == false) {
                // not even occupied
                if($this->playerOnLeft == $player) {
                    if($x <= 2) {
                        // he can recruit
                        return true;
                    }
                }
                else if(x >= 18) {
                    // he can recruit
                    return true;
                }
            }
        }
    }

    // todo leave it atm
    private function canMove($from, $to, $player) {

    }

    public static function emptyResultArray() {
        $ra = array();
        $ra["processedMoves"] = array();
        $ra["processedMoves"][0]["ID"]            = 0;
        $ra["processedMoves"][0]["type"]          = "unknown";
        $ra["processedMoves"][0]["data1"]         = "";
        $ra["processedMoves"][0]["data2"]         = "";
        $ra["processedMoves"][0]["data3"]         = "";
        $ra["processedMoves"][0]["data4"]         = "";
        $ra["processedMoves"][0]["data5"]         = "";
        $ra["processedMoves"][0]["extra_data"]    = "";
        $ra["errors"] = array();
        return $ra;
    }
}