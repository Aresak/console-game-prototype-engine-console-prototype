<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 28.05.2016
 * Time: 11:27
 */

$GENKEY = "ekyVW6QxwyzGJFZDNxFx2EV9J9YgqN4twRRpv9i7PvZv7NjKkSbtRfcnK85gGXArxEYDJsLCnMFC774eUbdL7j9Hj6vC48zAGux8";



function generateCharacter() {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = $characters[rand(0, $charactersLength - 1)];
    return $randomString;
}

function generateNumber() {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = $characters[rand(0, $charactersLength - 1)];
    return $randomString;
}

$patternUsual   = "BAAAA-AAAAA-AAAAA-ANNN"; // Usual Free Game Key  0
$patternAdmin   = "ZAAAA-AAAAA-AAAAA-ANNN"; // Admin Game Key       1
$patternMasta   = "MAAAA-AAAAA-AAAAA-ANNN"; // Game Master Game Key 2
$patternFree    = "FAAAA-AAAAA-AAAAA-ANNN"; // Give Away Game Key   3
$patternPayed   = "PAAAA-AAAAA-AAAAA-ANNN"; // Premium Game Key     4

// A - Character or Number
// N - Number
// C - Character

function generateSerialKey($pattern) {
    $result = "";

    for($i = 0; $i < strlen($pattern); $i ++) {
        switch($pattern[$i]) {
            case "A":
                $result .= (rand(0, 1) ? generateCharacter() : generateNumber());
                break;
            case "N":
                $result .= generateNumber();
                break;
            case "C":
                $result .= generateCharacter();
                break;
            default:
                $result .= $pattern[$i];
                break;
        }
    }

    return $result;
}

function generateSerialKeys($pattern, $count) {
    $result = array();

    for($i = 0; $i < $count; $i ++) {
        $result[$i] = generateSerialKey($pattern);
    }

    return $result;
}

function printout($finalized) {
    if($_GET["print"] == "JSON") {
        die(json_encode($finalized));
    }
    else {
        // text
        echo "<table align='center' border='1px solid black'>";
        echo "<tr>";
        echo "<td><b>Index</b></td><td><b>Serial Key Code</b></td><td><b>Public Key Code</b></td><td><b>Privilage Speciality</b></td>";
        if(isset($finalized[0]["owner_id"])) echo "<td><b>Owner ID</b></td>";
        echo "</tr>";
        for($i = 0; $i < count($finalized); $i ++) {
            echo "<tr>";
            echo "<td>$i</td>";
            echo "<td>" . $finalized[$i]["code"] . "</td>";
            echo "<td>" . $finalized[$i]["key"] . "</td>";
            echo "<td>" . $finalized[$i]["speciality"] . "</td>";
            if(isset($finalized[$i]["owner_id"])) echo "<td>" . $finalized[$i]["owner_id"] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
}

if($_GET["GENKEY"] == $GENKEY) {
    // Generate key
    $privilage = $_GET["PRIV"];
    $count = $_GET["COUNT"];
    $displayOnly = false;
    define("allowed_to_view_database_info", true);
    include "../../../database.php";

    // prepare the connection
    $con = mysqli_connect(SQLInfo::$host, SQLInfo::$user, SQLInfo::$pass, SQLInfo::$base)
    or die(mysqli_error($con));

    if($_GET["showKeys"]) {
        $query = "SELECT * FROM bft_gamekeys";
        $result = mysqli_query($con, $query) or die(mysqli_error($con));

        $finalized = array();
        for($i = 0; $i < mysqli_num_rows($result); $i ++) {
            $finalized[$i]["code"] = mysqli_result($result, $i, "key");
            $finalized[$i]["key"] = mysqli_result($result, $i, "public_key");
            $finalized[$i]["speciality"] = mysqli_result($result, $i, "speciality");
            $finalized[$i]["owner_id"]  = mysqli_result($result, $i, "owner_id");
        }
        printout($finalized);
        mysqli_close($con);
        die("Printed");
    }


    if(isset($_GET["DISPLAY_ONLY"])) $displayOnly = true;
    if(!isset($_GET["PRIV"])) die("Missing PRIV arg");
    if(!isset($_GET["COUNT"])) die("Missing COUNT arg");


    // generate keys
    $pattern = "";
    $spec = 0;
    switch($privilage) {
        case "Premium":
            $pattern = $patternPayed;
            $spec = 4;
            break;
        case "Admin":
            $pattern = $patternAdmin;
            $spec = 1;
            break;
        case "GameMaster":
            $pattern = $patternMasta;
            $spec = 2;
            break;
        case "GiveAway":
            $pattern = $patternFree;
            $spec = 3;
            break;
        default:
            $pattern = $patternUsual;
            $spec = 0;
            break;
    }

    $keys = generateSerialKeys($pattern, $count);

    $finalized = array();
    if(!$displayOnly) {
        // add to the database
        for($i = 0; $i < count($keys); $i ++) {
            $finalized[$i]["code"] = $keys[$i];
            $finalized[$i]["key"]  = generateKey(16);
            $finalized[$i]["speciality"] = $spec;

            $query = "INSERT INTO bft_gamekeys (`key`, public_key, speciality) VALUES ('" . $keys[$i] . "', '" . $finalized[$i]["key"] . "', '$spec')";
            $result = mysqli_query($con, $query)
                or die(mysqli_error($con));
        }
    }
    else {
        for($i = 0; $i < count($keys); $i ++) {
            $finalized[$i]["code"] = $keys[$i];
            $finalized[$i]["key"]  = generateKey(16);
            $finalized[$i]["speciality"] = $spec;
        }
    }

    mysqli_close($con);
    // print out
    printout($finalized);
} else {
    die("False Gen Key (" . $_GET["GENKEY"] . ")");
}